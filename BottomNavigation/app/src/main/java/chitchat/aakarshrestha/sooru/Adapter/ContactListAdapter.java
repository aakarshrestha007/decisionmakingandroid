package chitchat.aakarshrestha.sooru.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import chitchat.aakarshrestha.sooru.Helper.FriendContact;
import chitchat.aakarshrestha.sooru.Helper.Utility;
import chitchat.aakarshrestha.bottomnavigation.R;

public class ContactListAdapter extends BaseAdapter {

    public Context mContext;
    public List<FriendContact> mfriendContactList = new ArrayList<>();
    public LayoutInflater mInflater;
    public ViewHolder viewHolder;
    public List<FriendContact> friend_contact_list;

    public ContactListAdapter(Context context, List<FriendContact> friendContactList) {
        mContext = context;
        mfriendContactList = friendContactList;
        mInflater = LayoutInflater.from(mContext);
        friend_contact_list = new ArrayList<>();
        friend_contact_list.addAll(friendContactList);
    }

    @Override
    public int getCount() {
        return mfriendContactList.size();
    }

    @Override
    public Object getItem(int position) {
        return mfriendContactList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.contact_list_view, null);
            viewHolder = new ViewHolder();
            viewHolder.friendImageView = (ImageView) convertView.findViewById(R.id.friendImageListViewID);
            viewHolder.friendNameTextView = (TextView) convertView.findViewById(R.id.friendNameListViewID);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        FriendContact friendContact = mfriendContactList.get(position);

        String friendName = friendContact.getFriendName();
        viewHolder.friendNameTextView.setText(friendName);
//        viewHolder.friendNameTextView.setTextColor(Color.BLACK);

        Utility.changeTextColor(viewHolder.friendNameTextView, viewHolder.friendNameTextView.getContext());

        String imageOfContact = friendContact.getFriendImage();

        if (imageOfContact == null) {
            Picasso.with(mContext).load(R.drawable.dummy).into(viewHolder.friendImageView);
        } else {
            Picasso.with(mContext).load(imageOfContact).into(viewHolder.friendImageView);
        }

        return convertView;
    }

    static class ViewHolder {

        public ImageView friendImageView;
        public TextView friendNameTextView;

    }

    /*
        Filter listview items
     */
    public void filter(String charText) {

        charText = charText.toLowerCase(Locale.getDefault());
        mfriendContactList.clear();

        if (charText.length() == 0) {
            mfriendContactList.addAll(friend_contact_list);
        } else {

            for (FriendContact fc : friend_contact_list) {

                if (fc.getFriendName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    mfriendContactList.add(fc);
                }

            }

        }
        notifyDataSetChanged();

    }

}


























