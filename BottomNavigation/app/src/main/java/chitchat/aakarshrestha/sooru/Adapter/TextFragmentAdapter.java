package chitchat.aakarshrestha.sooru.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import chitchat.aakarshrestha.sooru.Helper.TextMessage;
import chitchat.aakarshrestha.bottomnavigation.R;

public class TextFragmentAdapter extends BaseAdapter{

    Context mContext;
    private List<TextMessage> mListOfTextMessage = new ArrayList<>();
    private LayoutInflater mInflater;
    private String senderPhoneNumber;

    public TextFragmentAdapter(Context mContext, List<TextMessage> mListOfTextMessage, String senderPhoneNumber) {
        this.mContext = mContext;
        this.mListOfTextMessage = mListOfTextMessage;
        this.senderPhoneNumber = senderPhoneNumber;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mListOfTextMessage.size();
    }

    @Override
    public Object getItem(int position) {
        return mListOfTextMessage.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;

        if (convertView == null) {
            if (senderPhoneNumber != null) {
                if (mListOfTextMessage.get(position).getSenderPhone().equals(senderPhoneNumber)) {
                    convertView = mInflater.inflate(R.layout.row_right, parent, false);
                } else {
                    convertView = mInflater.inflate(R.layout.row_left, parent, false);
                }
            }

            viewHolder = new ViewHolder();
            assert convertView != null;
            viewHolder.mMessageText = (TextView) convertView.findViewById(R.id.message);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        TextMessage txtmessage = mListOfTextMessage.get(position);
        String msg = txtmessage.getMessage();
        viewHolder.mMessageText.setText(msg);

        return convertView;
    }

    static class ViewHolder {
        TextView mMessageText;
    }


}
