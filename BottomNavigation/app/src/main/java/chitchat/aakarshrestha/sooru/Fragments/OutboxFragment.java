package chitchat.aakarshrestha.sooru.Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import chitchat.aakarshrestha.sooru.Adapter.ProfileMessageListAdapter;
import chitchat.aakarshrestha.sooru.Helper.Config;
import chitchat.aakarshrestha.sooru.Helper.ProfileMessageList;
import chitchat.aakarshrestha.bottomnavigation.R;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class OutboxFragment extends Fragment {

    private FloatingActionButton outbox_goup_notif_float_button;
    private SharedPreferences mSharedPrefMyNumber;
    private SharedPreferences mSharedPreferences;
    private OkHttpClient okHttpClient;
    private List<ProfileMessageList> listOfMessageAndSenderName = new ArrayList<>();
    private ProfileMessageListAdapter profileMessageListAdapter;
    private ListView mListview;
    private String[] questionAnswerArray = new String[1];
    private IntentFilter mConfirmIntentFilter;
    private ProgressBar progressBar;

    BroadcastReceiver mConfirmBroadCastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            listOfMessageAndSenderName.clear();

            ProfileMessageListTaskOutbox ProfileMessageListTaskOutbox  = new ProfileMessageListTaskOutbox();

            mSharedPreferences = getActivity().getSharedPreferences(Config.SHAREDPREFERENCES_MY_PHONE_NUMBER, Context.MODE_PRIVATE);
            final String myPhoneNumber = mSharedPreferences.getString(Config.MY_PHONE_NUMBER_KEY, "");

            ProfileMessageListTaskOutbox.execute(myPhoneNumber);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_outbox, container, false);
        outbox_goup_notif_float_button = (FloatingActionButton) view.findViewById(R.id.outbox_goup_notif_floatingButton_ID);
        outbox_goup_notif_float_button.setVisibility(View.GONE);
        mListview = (ListView) view.findViewById(R.id.outbox_listView_ID);
        okHttpClient = new OkHttpClient();

        /*
            Progress bar
         */
        progressBar = (ProgressBar) view.findViewById(R.id.outboxProgressBarID);
        progressBar.setVisibility(View.INVISIBLE);

        /*
        Add the action sent from the broadcast from MyFirebaseMessagingService file.
         */
        mConfirmIntentFilter = new IntentFilter();
        mConfirmIntentFilter.addAction(Config.FCM_CONFIRM_RECEIVED_ACTION);

        /*
        Register the broadcast receiver
         */
        getActivity().registerReceiver(mConfirmBroadCastReceiver, mConfirmIntentFilter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        /*
        Register the broadcast receiver
         */
        getActivity().registerReceiver(mConfirmBroadCastReceiver, mConfirmIntentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(mConfirmBroadCastReceiver);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSharedPrefMyNumber = getActivity().getSharedPreferences(Config.SHAREDPREFERENCES_MY_PHONE_NUMBER, Context.MODE_PRIVATE);
        final String my_phone_number = mSharedPrefMyNumber.getString(Config.MY_PHONE_NUMBER_KEY, "");

        final ProfileMessageListTaskOutbox profileMessageListTaskOne  = new ProfileMessageListTaskOutbox();
        /*
        Load the message you sent to others when profilefragment is loaded.
         */
        Timer timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        profileMessageListTaskOne.execute(my_phone_number);
                    }
                });
            }
        };
        timer.schedule(timerTask, 55);

        mListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String listItem = mListview.getItemAtPosition(position).toString();
                String[] listItemArray = listItem.split("\n");
                String question = listItemArray[2];
                final String receiverName = listItemArray[0];
                String receiverPhoneNumber = listItemArray[4];

                String phoneNumber;
                if (receiverPhoneNumber.equals("null")) {
                    phoneNumber = listItemArray[3];
                } else {
                    phoneNumber = receiverPhoneNumber;
                }

                /*
                Make a web service call to get question and answers
                 */
                mSharedPreferences = getActivity().getSharedPreferences(Config.SHAREDPREFERENCES_MY_PHONE_NUMBER, Context.MODE_PRIVATE);
                String myPhoneNumber = mSharedPreferences.getString(Config.MY_PHONE_NUMBER_KEY, "");

                GetQuestionAnswersTask getQuestionAnswersTask = new GetQuestionAnswersTask();
                getQuestionAnswersTask.execute(myPhoneNumber, phoneNumber, question, receiverName);
            }
        });

        mListview.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                if (mListview.getFirstVisiblePosition() != 0) {
                    outbox_goup_notif_float_button.setVisibility(View.VISIBLE);
                } else {
                    outbox_goup_notif_float_button.setVisibility(View.INVISIBLE);
                }

            }
        });

        outbox_goup_notif_float_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListview.smoothScrollToPosition(0);
            }
        });

    }

    private class GetQuestionAnswersTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            getQuestionAnswers(params[0], params[1], params[2], params[3]);
            return null;
        }
    }


    private void getQuestionAnswers(String myPhoneNumber, String receiverPhoneNumber, String question, final String receiverName) {
        Request request = new Request.Builder()
                .url(Config.showSentQuestionAnswer(myPhoneNumber, receiverPhoneNumber, question))
                .build();

        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {
                    String mResponse = response.body().string();
                    try {
                        JSONArray jsonArray = new JSONArray(mResponse);
                        for (int i=0; i<jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String mQuestion = jsonObject.getString("question");
                            String mOption_one = jsonObject.getString("option_one");
                            String mOption_two = jsonObject.getString("option_two");
                            String mOption_three = jsonObject.getString("option_three");
                            String mOption_four = jsonObject.getString("option_four");
                            String picked_option = jsonObject.getString("option_picked");

                            questionAnswerArray[i] = mQuestion + "-:" + mOption_one + "-:" + mOption_two + "-:" + mOption_three + "-:" + mOption_four + "-:" + picked_option;

                            String[] mQuesAns = questionAnswerArray[0].split("\\?\\-:");
                            final String question = mQuesAns[0]+"?";
                            String[] answers = mQuesAns[1].split("\\-:");
                            final String option_one = answers[0];
                            final String option_two = answers[1];
                            final String option_three = answers[2];
                            final String option_four = answers[3];
                            final String option_picked = answers[4];

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                    LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                                    View mView = layoutInflater.inflate(R.layout.ques_ans_alert_box, null);
                                    TextView receiverNameTextView = (TextView) mView.findViewById(R.id.nameOfUserID);
                                    TextView questionTextView = (TextView) mView.findViewById(R.id.user_question_ID);
                                    TextView optionOneTextView = (TextView) mView.findViewById(R.id.ques_ans_option_one_id);
                                    TextView optionTwoTextView = (TextView) mView.findViewById(R.id.ques_ans_option_two_id);
                                    TextView optionThreeTextView = (TextView) mView.findViewById(R.id.ques_ans_option_three_id);
                                    TextView optionFourTextView = (TextView) mView.findViewById(R.id.ques_ans_option_four_id);

                                    receiverNameTextView.setText(receiverName);
                                    questionTextView.setText(question);
                                    optionOneTextView.setText(option_one);
                                    optionTwoTextView.setText(option_two);
                                    optionThreeTextView.setText(option_three);
                                    optionFourTextView.setText(option_four);

                                    Button okButton = (Button) mView.findViewById(R.id.ques_ans_ok_id);
                                    builder.setView(mView);

                                    final AlertDialog dialog = builder.create();

                                    if (option_picked.equals(optionOneTextView.getText())) {
                                        optionOneTextView.setBackgroundColor(getResources().getColor(R.color.vineGreen));
                                        optionOneTextView.setTextColor(Color.WHITE);
                                        YoYo.with(Techniques.FlipOutX).duration(999).playOn(optionOneTextView);
                                        YoYo.with(Techniques.FlipInX).duration(999).playOn(optionOneTextView);
                                    } else if (option_picked.equals(optionTwoTextView.getText())) {
                                        optionTwoTextView.setBackgroundColor(getResources().getColor(R.color.vineGreen));
                                        optionTwoTextView.setTextColor(Color.WHITE);
                                        YoYo.with(Techniques.FlipOutX).duration(999).playOn(optionTwoTextView);
                                        YoYo.with(Techniques.FlipInX).duration(999).playOn(optionTwoTextView);
                                    } else if (option_picked.equals(optionThreeTextView.getText())) {
                                        optionThreeTextView.setBackgroundColor(getResources().getColor(R.color.vineGreen));
                                        optionThreeTextView.setTextColor(Color.WHITE);
                                        YoYo.with(Techniques.FlipOutX).duration(999).playOn(optionThreeTextView);
                                        YoYo.with(Techniques.FlipInX).duration(999).playOn(optionThreeTextView);
                                    } else if (option_picked.equals(optionFourTextView.getText())) {
                                        optionFourTextView.setBackgroundColor(getResources().getColor(R.color.vineGreen));
                                        optionFourTextView.setTextColor(Color.WHITE);
                                        YoYo.with(Techniques.FlipOutX).duration(999).playOn(optionFourTextView);
                                        YoYo.with(Techniques.FlipInX).duration(999).playOn(optionFourTextView);
                                    }

                                    okButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            if (dialog != null && dialog.isShowing()) {
                                                dialog.dismiss();
                                            }
                                        }
                                    });

                                    dialog.show();

                                }
                            });

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        });
    }

    private class ProfileMessageListTaskOutbox extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {

            makeProfileFragmentMessageWebServiceCallFrom(params[0]);
            return null;
        }
    }

    private void makeProfileFragmentMessageWebServiceCallFrom(final String myPhoneNumber) {

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.VISIBLE);
            }
        });

        final Request request = new Request.Builder()
                .url(Config.URL_PROFILE_MESSAGE_LIST + myPhoneNumber)
                .build();

        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    });

                    try {
                        String mResponse = response.body().string();
                        JSONArray jsonArray = new JSONArray(mResponse);

                        for (int i=0; i<jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String senderPhone = jsonObject.getString("user_one_phone");
                            String senderName = jsonObject.getString("sender_name");
                            String receiverName = jsonObject.getString("receiver_name");
                            String receiverPhone = jsonObject.getString("user_two_phone");
                            String questionFromUser = jsonObject.getString("question");
                            String createdDate = jsonObject.getString("createdDate");
                            String picked_option = jsonObject.getString("option_picked");

                            ProfileMessageList profileMessageList = new ProfileMessageList();
                            profileMessageList.setSenderName(/*"You to " + */ receiverName);
                            profileMessageList.setQuestion(questionFromUser);
                            profileMessageList.setCreatedDate(createdDate);
                            profileMessageList.setOption_picked(picked_option);
                            profileMessageList.setReceiverPhoneNumber(receiverPhone);

                            listOfMessageAndSenderName.add(profileMessageList);
                        }

                        /*
                        get the custom adapter and set the listview
                         */
                        getProfileMessageListAdapter();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        });

    }

    private void getProfileMessageListAdapter() {

        mSharedPreferences = getActivity().getSharedPreferences(Config.SHAREDPREFERENCES_MY_PHONE_NUMBER, Context.MODE_PRIVATE);
        final String myPhoneNumber = mSharedPreferences.getString(Config.MY_PHONE_NUMBER_KEY, "");

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                profileMessageListAdapter = new ProfileMessageListAdapter(getContext().getApplicationContext(), listOfMessageAndSenderName, myPhoneNumber);
                mListview.setAdapter(profileMessageListAdapter);
                profileMessageListAdapter.notifyDataSetChanged();


                /*
                Animation to load one item at a time in listview
                 */
                LayoutAnimationController lac = new LayoutAnimationController(AnimationUtils.loadAnimation(getActivity(), R.anim.enter_from_right), 1);
                mListview.setLayoutAnimation(lac);
                mListview.startLayoutAnimation();
            }
        });
    }

}
