package chitchat.aakarshrestha.sooru.Helper;

public class MessageFromFriend {

    String question, option_one, option_two, option_three, option_four;

    public MessageFromFriend(String question, String option_one, String option_two, String option_three, String option_four) {
        this.question = question;
        this.option_one = option_one;
        this.option_two = option_two;
        this.option_three = option_three;
        this.option_four = option_four;
    }

    public MessageFromFriend() {
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getOption_one() {
        return option_one;
    }

    public void setOption_one(String option_one) {
        this.option_one = option_one;
    }

    public String getOption_two() {
        return option_two;
    }

    public void setOption_two(String option_two) {
        this.option_two = option_two;
    }

    public String getOption_three() {
        return option_three;
    }

    public void setOption_three(String option_three) {
        this.option_three = option_three;
    }

    public String getOption_four() {
        return option_four;
    }

    public void setOption_four(String option_four) {
        this.option_four = option_four;
    }

    @Override
    public String toString() {
        return question + "\n"
                + option_one + "\n"
                + option_two + "\n"
                + option_three + "\n"
                + option_four;
    }
}
