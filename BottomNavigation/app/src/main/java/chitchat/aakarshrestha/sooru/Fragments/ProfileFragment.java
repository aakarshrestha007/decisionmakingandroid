package chitchat.aakarshrestha.sooru.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.squareup.picasso.Picasso;

import java.util.Timer;
import java.util.TimerTask;

import chitchat.aakarshrestha.sooru.Helper.Config;
import chitchat.aakarshrestha.sooru.Helper.Utility;
import chitchat.aakarshrestha.bottomnavigation.R;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFragment extends Fragment {

    protected CircleImageView circleImageView;
    protected TextView mProfileName;
    protected AccessTokenTracker accessTokenTracker;

    public SharedPreferences mSharedPreferences;
    private CoordinatorLayout profileFragmentLayout;

    private ViewPager viewPager = null;
    private TabLayout tabLayout = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.profile_fragment, container, false);

        circleImageView = (CircleImageView) view.findViewById(R.id.myFaceBookProfileimageID);
        mProfileName = (TextView) view.findViewById(R.id.profileNameID);

        viewPager = (ViewPager) view.findViewById(R.id.viewpagerID);
        tabLayout = (TabLayout) view.findViewById(R.id.tablayoutID);
        tabLayout.setupWithViewPager(viewPager);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        viewPager.setAdapter(new ViewPagerAdapter(fragmentManager));

        ProfileFragmentTask profileFragmentTask = new ProfileFragmentTask();
        profileFragmentTask.execute();

        /*
        AccessTokenTracker to manage the logout
         */

        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                if (currentAccessToken == null) {
                    mProfileName.setText("");
                    circleImageView.setImageResource(R.drawable.dummy);
                }
            }
        };

        /*
        Change the background color
         */
        profileFragmentLayout = (CoordinatorLayout) view.findViewById(R.id.profile_fragment_background);
        Utility.changeBackgroundColor(profileFragmentLayout, getContext());

        return view;
    }

    class ProfileFragmentTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            /*
            Get the intent from the loginactivity - name and imageurl info
            */

            Intent intent = getActivity().getIntent();
            final String profileName = intent.getStringExtra(Config.FACEBOOK_NAME);
            final String profileImage = intent.getStringExtra(Config.FACEBOOK_PROFILE_IMAGE);

            mSharedPreferences = getActivity().getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
            final String profileNameFromMSGRoomActivity = mSharedPreferences.getString(Config.SHAREDPREFERENCES_NAME,"");
            final String profileImageFromMSGRoomActivity = mSharedPreferences.getString(Config.SHAREDPREFERENCES_IMAGE, "");

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mProfileName.setText(profileName);

                    if (profileImage == null && profileName == null) {

                        if (profileNameFromMSGRoomActivity != null && profileImageFromMSGRoomActivity != null) {
                            mProfileName.setText(profileNameFromMSGRoomActivity);
                            Picasso.with(getContext()).load(profileImageFromMSGRoomActivity).into(circleImageView);
                        } else {
                            Picasso.with(getContext()).load(R.drawable.dummy).into(circleImageView);
                        }

                    } else {
                        Picasso.with(getContext()).load(profileImage).into(circleImageView);
                    }

                }
            });


            return null;
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Timer timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                circleImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                String profileImageFromMSGRoomActivity = mSharedPreferences.getString(Config.SHAREDPREFERENCES_IMAGE, "");

                                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                                View mView = layoutInflater.inflate(R.layout.profile_image_in_alert, null);
                                CircleImageView circleImageView = (CircleImageView) mView.findViewById(R.id.profile_image_in_alert_ID);
                                Picasso.with(getContext()).load(profileImageFromMSGRoomActivity).into(circleImageView);
                                builder.setView(mView);

                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }
                        });
                    }
                });
            }
        };
        timer.schedule(timerTask, 55);
    }
}

class ViewPagerAdapter extends FragmentStatePagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new InboxFragment();
                break;
            case 1:
                fragment = new OutboxFragment();
                break;
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        String tabNames = null;
        if (position == 0) {
            tabNames = "Inbox";
        }
        if (position == 1) {
            tabNames = "Outbox";
        }

        return tabNames;
    }
}
