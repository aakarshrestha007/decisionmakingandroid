package chitchat.aakarshrestha.sooru.Helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper{

    private static final String DATABASE_NAME = "Notifications.db";

    private static final String QUESTION_NOTIFICATION_TABLE_NAME = "AskNotificationTable";
    private static final String CONTACTS_COLUMN_ID = "id";
    private static final String CONTACTS_COLUMN_NAME = "username";
    private static final String CONTACTS_COLUMN_NOTIFICATION = "notification";

    private static final String ANSWER_NOTIFICATION_TABLE_NAME = "AnswerNotificationTable";
    private static final String ANSWER_COLUMN_ID = "id";
    private static final String ANSWER_COLUMN_NAME = "username";
    private static final String ANSWER_COLUMN = "answer";
    private static final String QUESTION_COLUMN = "question";

    private static final String TEXT_MESSAGE_TABLE_NAME = "TextMessageTable";
    private static final String TEXT_MESSAGE_ID = "id";
    private static final String TEXT_MESSAGE_SENDERNAME_COLUMN = "sendername";
    private static final String TEXT_MESSAGE_TEXTMESSAGE_COLUMN = "textmessage";

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String questionSqlQuery = "create table IF NOT EXISTS " + QUESTION_NOTIFICATION_TABLE_NAME + " (" + CONTACTS_COLUMN_ID + " integer primary key autoincrement," +
                CONTACTS_COLUMN_NAME + " text, " + CONTACTS_COLUMN_NOTIFICATION + " text"+ ")";

        String answerSqlQuery = "create table IF NOT EXISTS " + ANSWER_NOTIFICATION_TABLE_NAME + " (" + ANSWER_COLUMN_ID + " integer primary key autoincrement," +
                ANSWER_COLUMN_NAME + " text, " + ANSWER_COLUMN + " text, " + QUESTION_COLUMN +" text" + ")";

        String textMessageSqlQuery = "create table IF NOT EXISTS " + TEXT_MESSAGE_TABLE_NAME + " (" + TEXT_MESSAGE_ID + " integer primary key autoincrement," +
                TEXT_MESSAGE_SENDERNAME_COLUMN + " text, " + TEXT_MESSAGE_TEXTMESSAGE_COLUMN + " text" + ")";

        db.execSQL(questionSqlQuery);
        db.execSQL(answerSqlQuery);
        db.execSQL(textMessageSqlQuery);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + QUESTION_NOTIFICATION_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ANSWER_NOTIFICATION_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TEXT_MESSAGE_TABLE_NAME);
        onCreate(db);

    }

    public boolean insertAskNotifications(String username, String notification_message) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CONTACTS_COLUMN_NAME, username);
        contentValues.put(CONTACTS_COLUMN_NOTIFICATION, notification_message);
        long result = db.insert(QUESTION_NOTIFICATION_TABLE_NAME, null, contentValues);

        if (result == -1) {
            return false;
        } else {
            return true;
        }

    }

    public Cursor getAllNotification() {

        SQLiteDatabase db = this.getWritableDatabase();
        String sqlQuery = "SELECT "+ CONTACTS_COLUMN_NAME + ", " + CONTACTS_COLUMN_NOTIFICATION +" FROM " + QUESTION_NOTIFICATION_TABLE_NAME +
                " ORDER BY " + CONTACTS_COLUMN_ID + " DESC";
        Cursor res = db.rawQuery(sqlQuery, null);

        return res;

    }

    public void deleteAskNotificationTable() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + QUESTION_NOTIFICATION_TABLE_NAME);
        onCreate(db);

    }

    public boolean insertAnswerNotifications(String username, String answer, String question) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ANSWER_COLUMN_NAME, username);
        contentValues.put(ANSWER_COLUMN, answer);
        contentValues.put(QUESTION_COLUMN, question);
        long result = db.insert(ANSWER_NOTIFICATION_TABLE_NAME, null, contentValues);

        if (result == -1) {
            return false;
        } else {
            return true;
        }

    }

    public Cursor getAllAnswerNotification() {

        SQLiteDatabase db = this.getWritableDatabase();
        String sqlQuery = "SELECT " + ANSWER_COLUMN_NAME + ", " + ANSWER_COLUMN + " FROM " + ANSWER_NOTIFICATION_TABLE_NAME +
                " ORDER BY " + ANSWER_COLUMN_ID + " DESC";
        Cursor res = db.rawQuery(sqlQuery, null);
        return res;

    }

    public void deleteAnswerNotificationTable() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + ANSWER_NOTIFICATION_TABLE_NAME);
        onCreate(db);

    }

    public boolean insertTextMessageNotification(String senderName, String textMessage) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TEXT_MESSAGE_SENDERNAME_COLUMN, senderName);
        contentValues.put(TEXT_MESSAGE_TEXTMESSAGE_COLUMN, textMessage);
        long result = db.insert(TEXT_MESSAGE_TABLE_NAME, null, contentValues);

        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Cursor getAllTextMessageNotificaton() {

        SQLiteDatabase db = this.getWritableDatabase();
        String sqlQuery = "SELECT " + TEXT_MESSAGE_SENDERNAME_COLUMN + ", " + TEXT_MESSAGE_TEXTMESSAGE_COLUMN + " FROM " + TEXT_MESSAGE_TABLE_NAME +
                " ORDER BY " + TEXT_MESSAGE_ID + " DESC";
        Cursor res = db.rawQuery(sqlQuery, null);
        return res;
    }

    public void deleteTextMessageNotification() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TEXT_MESSAGE_TABLE_NAME);
        onCreate(db);
    }

}
