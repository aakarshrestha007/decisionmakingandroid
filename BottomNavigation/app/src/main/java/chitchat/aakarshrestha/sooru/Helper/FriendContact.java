package chitchat.aakarshrestha.sooru.Helper;

public class FriendContact {

    String friendName, friendPhoneNumber, friendImage, friendStatus, friendFcmToken, notificationIconStatus;

    public FriendContact() {
    }

    public FriendContact(String friendName, String friendPhoneNumber, String friendImage, String friendStatus, String friendFcmToken) {
        this.friendName = friendName;
        this.friendPhoneNumber = friendPhoneNumber;
        this.friendImage = friendImage;
        this.friendStatus = friendStatus;
        this.friendFcmToken = friendFcmToken;
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public String getFriendPhoneNumber() {
        return friendPhoneNumber;
    }

    public void setFriendPhoneNumber(String friendPhoneNumber) {
        this.friendPhoneNumber = friendPhoneNumber;
    }

    public String getFriendImage() {
        return friendImage;
    }

    public void setFriendImage(String friendImage) {
        this.friendImage = friendImage;
    }

    public String getFriendStatus() {
        return friendStatus;
    }

    public void setFriendStatus(String friendStatus) {
        this.friendStatus = friendStatus;
    }

    public String getFriendFcmToken() {
        return friendFcmToken;
    }

    public void setFriendFcmToken(String friendFcmToken) {
        this.friendFcmToken = friendFcmToken;
    }

    @Override
    public String toString() {
        return friendName + '\n' +friendPhoneNumber + '\n' + friendStatus + '\n' + friendFcmToken;
    }
}
