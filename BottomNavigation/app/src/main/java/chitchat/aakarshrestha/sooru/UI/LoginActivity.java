package chitchat.aakarshrestha.sooru.UI;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import chitchat.aakarshrestha.sooru.Helper.Config;
import chitchat.aakarshrestha.sooru.Helper.DataBaseHelper;
import chitchat.aakarshrestha.sooru.Helper.Utility;
import chitchat.aakarshrestha.bottomnavigation.R;

public class LoginActivity extends AppCompatActivity {

    private LoginButton loginButton;
    private ImageButton fbLoginButton;
    private CallbackManager callbackManager = CallbackManager.Factory.create();

    public SharedPreferences mSharedPreferences;
    public SharedPreferences.Editor mEditor;

    DataBaseHelper dataBaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);

        /*
        Create the SQLite DB
         */
        dataBaseHelper = new DataBaseHelper(LoginActivity.this);

        printKeyHash(LoginActivity.this);

        loginButton = (LoginButton) findViewById(R.id.login_button);
        fbLoginButton = (ImageButton) findViewById(R.id.custom_fb_button);

        fbLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Utility.isConnectedToNetwork(getApplicationContext())) {
                    loginButton.performClick();

                    loginButton.setReadPermissions(Arrays.asList("public_profile", "email"));
                    loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {
                            //goMainScreen();

                /*
                Get user profile picture and name
                 */

                            GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                                    new GraphRequest.GraphJSONObjectCallback() {
                                        @Override
                                        public void onCompleted(JSONObject object, GraphResponse response) {
                                            try {

                                                String name = object.getString("name");
                                                String id = object.getString("id");
                                                String imageUrl = "https://graph.facebook.com/" + id + "/picture?type=large";
//                                Log.v("NAME", name);
//                                Log.v("fbID", id);
//                                Log.v("IMAGEURL", imageUrl);

                                                mSharedPreferences = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
                                                mEditor = mSharedPreferences.edit();
                                                mEditor.putString(Config.SHAREDPREFERENCES_NAME, name);
                                                mEditor.putString(Config.SHAREDPREFERENCES_ID, id);
                                                mEditor.putString(Config.SHAREDPREFERENCES_IMAGE, imageUrl);
                                                mEditor.apply();

                                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                intent.putExtra(Config.FACEBOOK_NAME, name);
                                                intent.putExtra(Config.FACEBOOK_ID, id);
                                                intent.putExtra(Config.FACEBOOK_PROFILE_IMAGE, imageUrl);
                                                startActivity(intent);

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });

                            Bundle parameters = new Bundle();
                            parameters.putString("fields", "id,name");
                            request.setParameters(parameters);
                            request.executeAsync();
                        }

                        @Override
                        public void onCancel() {
                            Toast.makeText(getApplicationContext(), "Cancel Login", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onError(FacebookException error) {
                            Toast.makeText(getApplicationContext(), "Error while login", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Utility.showNetworkConnectionAlertMessage(LoginActivity.this, "Network Connectivity", "Please connect to the network.");
                        }
                    });

                }


            }
        });

        /*
        If the user has not logged out and swipes the app from the app list, user should not be logged out.
         */
        Utility.autoLogin(getApplicationContext());
    }

    private void goMainScreen() {

        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        }
        catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }

}