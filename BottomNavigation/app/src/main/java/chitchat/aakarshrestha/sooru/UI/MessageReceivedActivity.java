package chitchat.aakarshrestha.sooru.UI;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import chitchat.aakarshrestha.sooru.Helper.Config;
import chitchat.aakarshrestha.sooru.Helper.DataBaseHelper;
import chitchat.aakarshrestha.sooru.Helper.MessageFromFriend;
import chitchat.aakarshrestha.sooru.Helper.Utility;
import chitchat.aakarshrestha.bottomnavigation.R;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MessageReceivedActivity extends AppCompatActivity {

    protected ImageView cardImageView;
    protected ImageButton button0, button1, button2, button3;
    protected Button confirmButton;
    protected TextView textView, friendNameText, questionText;
    protected ImageView backArrowImageButton;
    protected CircleImageView friendCircleImageView;
    protected Snackbar snackbar;

    public OkHttpClient okHttpClient = new OkHttpClient();
    public SharedPreferences sharedPreferences;
    public List<MessageFromFriend> listOfMessageFromFriend;
    public SharedPreferences mSharedPref;

    boolean isEggCracked = false;

    protected boolean isOffButtonShowing0 = true;
    protected boolean isOffButtonShowing1 = true;
    private boolean isOffButtonShowing2 = true;
    private boolean isOffButtonShowing3 = true;

    String optionName = null;

    public static MessageReceivedActivity mMessageReceivedActivity;
    protected ConstraintLayout messageReceivedActivityLayout;

    public DataBaseHelper dataBaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_received);

        button0 = (ImageButton) findViewById(R.id.imageButton0);
        button1 = (ImageButton) findViewById(R.id.imageButton1);
        button2 = (ImageButton) findViewById(R.id.imageButton2);
        button3 = (ImageButton) findViewById(R.id.imageButton3);
        confirmButton = (Button) findViewById(R.id.confirmButtonID);
        backArrowImageButton = (ImageView) findViewById(R.id.messageReceivedBackArrowID);
        friendNameText = (TextView) findViewById(R.id.msgReceivedfriendNameID);
        friendCircleImageView = (CircleImageView) findViewById(R.id.msgReceivedfriendImageID);
        questionText = (TextView) findViewById(R.id.question_received_ID);

        backArrowImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToMainActivity();
            }
        });

        friendNameText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToMainActivity();
            }
        });

        mMessageReceivedActivity = this;

        /*
        Hide confirm button
         */
        confirmButton.setVisibility(View.INVISIBLE);

        cardImageView = (ImageView) findViewById(R.id.cardImageID);
        textView = (TextView) findViewById(R.id.textView);
        textView.setVisibility(View.GONE);

        messageReceivedActivityLayout = (ConstraintLayout) findViewById(R.id.message_received_activity_ID);
        Utility.changeBackgroundColor(messageReceivedActivityLayout, getApplicationContext());
        Utility.changeBackgroundColor(button0, getApplicationContext());
        Utility.changeBackgroundColor(button1, getApplicationContext());
        Utility.changeBackgroundColor(button2, getApplicationContext());
        Utility.changeBackgroundColor(button3, getApplicationContext());
        Utility.changeTextColor(textView, getApplicationContext());

        /*
        Media Player
         */
//        mediaPlayer = MediaPlayer.create(MessageReceivedActivity.this, R.raw.sound_effect);

        dataBaseHelper = new DataBaseHelper(MessageReceivedActivity.this);

        Intent intent = getIntent();
        String askNotificationKey = intent.getStringExtra("askNotificationKey");

        /*
        Delete the Sqlite DB if the value matches
         */
        if (askNotificationKey.equals("1")) {
            dataBaseHelper.deleteAskNotificationTable();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();

        Intent intent = getIntent();
        String friendName = intent.getStringExtra("friend_name_from_list");
        String friendPhoneNumber = intent.getStringExtra("friend_phone_number_from_list");
        String image_of_friend = intent.getStringExtra("image_of_friend");

        String askNotificationKey = intent.getStringExtra("askNotificationKey");

        /*
        Delete the Sqlite DB if the value matches
         */
        if (askNotificationKey.equals("1")) {
            dataBaseHelper.deleteAskNotificationTable();

            /*
            Remove all notification(s) from the status bar
             */
            NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancelAll();
        }


        friendNameText.setText(friendName);
        friendNameText.setTextColor(Color.WHITE);

        if (image_of_friend != null) {
            Picasso.with(getApplicationContext()).load(image_of_friend).into(friendCircleImageView);
        } else {
            Picasso.with(getApplicationContext()).load(R.drawable.dummy).into(friendCircleImageView);
        }

        listOfMessageFromFriend = new ArrayList<>();

        sharedPreferences = getSharedPreferences(Config.SHAREDPREFERENCES_MY_PHONE_NUMBER, Context.MODE_PRIVATE);
        String myPhoneNumber = sharedPreferences.getString(Config.MY_PHONE_NUMBER_KEY, "");

        /*
            Make a webservice call to get the message send by a friend
         */
        makeWebserviceCallToGetMessageFromFriend(friendPhoneNumber, myPhoneNumber);

    }

    public void navigateToMainActivity() {
        Intent intent = new Intent(MessageReceivedActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    public void crackTheEgg(String text) {
        if (!isEggCracked) {
            cardImageView.setImageResource(R.drawable.crackedeasteregg);
            YoYo.with(Techniques.DropOut).duration(700).playOn(cardImageView);
            isEggCracked = true;
            textView.setVisibility(View.VISIBLE);
            textView.setText(text);
            YoYo.with(Techniques.FadeOut).duration(10).playOn(textView);
            YoYo.with(Techniques.FadeIn).duration(900).playOn(textView);
            YoYo.with(Techniques.BounceInUp).duration(2000).playOn(textView);
        } else {
            textView.setVisibility(View.GONE);
            textView.setText("");
            isEggCracked = false;
        }
    }

    public void hideOptionButtons() {
        button0.setVisibility(View.INVISIBLE);
        button1.setVisibility(View.INVISIBLE);
        button2.setVisibility(View.INVISIBLE);
        button3.setVisibility(View.INVISIBLE);
    }

    public void showConfirmButton() {
        confirmButton.setVisibility(View.VISIBLE);
        confirmButton.setTextColor(Color.WHITE);
    }

    public void selectTheOption(final String optionOne, final String optionTwo, final String OptionThree, final String optionFour) {
        button0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = optionOne;

                cardImageView.setImageResource(R.drawable.eastergoldegg);
                YoYo.with(Techniques.Tada).duration(700).playOn(cardImageView);

                optionName = text;

                if (isOffButtonShowing0 && isOffButtonShowing1 && isOffButtonShowing2 && isOffButtonShowing3) {
                    button0.setImageResource(R.drawable.onbutton);
                    isOffButtonShowing0 = false;
                    showConfirmButton();
                } else if (isOffButtonShowing0 && !isOffButtonShowing1 && isOffButtonShowing2 && isOffButtonShowing3) {
                    button0.setImageResource(R.drawable.onbutton);
                    isOffButtonShowing0 = false;
                    button1.setImageResource(R.drawable.offbutton);
                    isOffButtonShowing1 = true;
                    showConfirmButton();
                } else if (!isOffButtonShowing0 && isOffButtonShowing1 && isOffButtonShowing2 && isOffButtonShowing3) {
                    button0.setImageResource(R.drawable.offbutton);
                    confirmButton.setVisibility(View.INVISIBLE);
                    isOffButtonShowing0 = true;
                } else if (isOffButtonShowing0 && isOffButtonShowing1 && !isOffButtonShowing2 && isOffButtonShowing3) {
                    button0.setImageResource(R.drawable.onbutton);
                    isOffButtonShowing0 = false;
                    button2.setImageResource(R.drawable.offbutton);
                    isOffButtonShowing2 = true;
                    showConfirmButton();
                } else if (isOffButtonShowing0 && isOffButtonShowing1 && isOffButtonShowing2 && !isOffButtonShowing3) {
                    button0.setImageResource(R.drawable.onbutton);
                    isOffButtonShowing0 = false;
                    button3.setImageResource(R.drawable.offbutton);
                    isOffButtonShowing3 = true;
                    showConfirmButton();
                }
                else {
                    button0.setImageResource(R.drawable.offbutton);
                    button1.setImageResource(R.drawable.offbutton);
                    button2.setImageResource(R.drawable.offbutton);
                    button3.setImageResource(R.drawable.offbutton);
                    isOffButtonShowing0 = true;
                    isOffButtonShowing1 = true;
                    isOffButtonShowing2 = true;
                    isOffButtonShowing3 = true;
                    showConfirmButton();
                }

            }

        });

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = optionTwo;

                cardImageView.setImageResource(R.drawable.eastergoldegg);
                YoYo.with(Techniques.Tada).duration(700).playOn(cardImageView);

                optionName = text;

                if (isOffButtonShowing1 && isOffButtonShowing0 && isOffButtonShowing2 && isOffButtonShowing3) {
                    button1.setImageResource(R.drawable.onbutton);
                    isOffButtonShowing1 = false;
                    showConfirmButton();
                } else if (isOffButtonShowing1 && !isOffButtonShowing0 && isOffButtonShowing2 && isOffButtonShowing3) {
                    button1.setImageResource(R.drawable.onbutton);
                    isOffButtonShowing1 = false;
                    button0.setImageResource(R.drawable.offbutton);
                    isOffButtonShowing0 = true;
                    showConfirmButton();
                } else if (!isOffButtonShowing1 && isOffButtonShowing0 && isOffButtonShowing2 && isOffButtonShowing3) {
                    button1.setImageResource(R.drawable.offbutton);
                    isOffButtonShowing1 = true;
                    confirmButton.setVisibility(View.INVISIBLE);
                } else if (isOffButtonShowing1 && isOffButtonShowing0 && !isOffButtonShowing2 && isOffButtonShowing3) {
                    button1.setImageResource(R.drawable.onbutton);
                    isOffButtonShowing1 = false;
                    button2.setImageResource(R.drawable.offbutton);
                    isOffButtonShowing2 = true;
                    showConfirmButton();
                } else if (isOffButtonShowing1 && isOffButtonShowing0 && isOffButtonShowing2 && !isOffButtonShowing3) {
                    button1.setImageResource(R.drawable.onbutton);
                    isOffButtonShowing1 = false;
                    button3.setImageResource(R.drawable.offbutton);
                    isOffButtonShowing3 = true;
                    showConfirmButton();
                }
                else {
                    button0.setImageResource(R.drawable.offbutton);
                    button1.setImageResource(R.drawable.offbutton);
                    button2.setImageResource(R.drawable.offbutton);
                    button3.setImageResource(R.drawable.offbutton);
                    isOffButtonShowing0 = true;
                    isOffButtonShowing1 = true;
                    isOffButtonShowing2 = true;
                    isOffButtonShowing3 = true;
                    showConfirmButton();
                }


            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = OptionThree;

                cardImageView.setImageResource(R.drawable.eastergoldegg);
                YoYo.with(Techniques.Tada).duration(700).playOn(cardImageView);

                optionName = text;

                if (isOffButtonShowing2 && isOffButtonShowing0 && isOffButtonShowing1 && isOffButtonShowing3) {
                    button2.setImageResource(R.drawable.onbutton);
                    isOffButtonShowing2 = false;
                    showConfirmButton();
                } else if (isOffButtonShowing2 && !isOffButtonShowing0 && isOffButtonShowing1 && isOffButtonShowing3) {
                    button2.setImageResource(R.drawable.onbutton);
                    isOffButtonShowing2 = false;
                    button0.setImageResource(R.drawable.offbutton);
                    isOffButtonShowing0 = true;
                    showConfirmButton();
                } else if (!isOffButtonShowing2 && isOffButtonShowing0 && isOffButtonShowing1 && isOffButtonShowing3) {
                    button2.setImageResource(R.drawable.offbutton);
                    isOffButtonShowing2 = true;
                    confirmButton.setVisibility(View.INVISIBLE);
                } else if (isOffButtonShowing2 && isOffButtonShowing0 && !isOffButtonShowing1 && isOffButtonShowing3) {
                    button2.setImageResource(R.drawable.onbutton);
                    isOffButtonShowing2 = false;
                    button1.setImageResource(R.drawable.offbutton);
                    isOffButtonShowing1 = true;
                    showConfirmButton();
                } else if (isOffButtonShowing2 && isOffButtonShowing0 && isOffButtonShowing1 && !isOffButtonShowing3) {
                    button2.setImageResource(R.drawable.onbutton);
                    isOffButtonShowing2 = false;
                    button3.setImageResource(R.drawable.offbutton);
                    isOffButtonShowing3 = true;
                    showConfirmButton();
                }
                else {
                    button0.setImageResource(R.drawable.offbutton);
                    button1.setImageResource(R.drawable.offbutton);
                    button2.setImageResource(R.drawable.offbutton);
                    button3.setImageResource(R.drawable.offbutton);
                    isOffButtonShowing0 = true;
                    isOffButtonShowing1 = true;
                    isOffButtonShowing2 = true;
                    isOffButtonShowing3 = true;
                    showConfirmButton();
                }

            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = optionFour;

                cardImageView.setImageResource(R.drawable.eastergoldegg);
                YoYo.with(Techniques.Tada).duration(700).playOn(cardImageView);

                optionName = text;

                if (isOffButtonShowing3 && isOffButtonShowing0 && isOffButtonShowing1 && isOffButtonShowing2) {
                    button3.setImageResource(R.drawable.onbutton);
                    isOffButtonShowing3 = false;
                    showConfirmButton();
                } else if (isOffButtonShowing3 && !isOffButtonShowing0 && isOffButtonShowing1 && isOffButtonShowing2) {
                    button3.setImageResource(R.drawable.onbutton);
                    isOffButtonShowing3 = false;
                    button0.setImageResource(R.drawable.offbutton);
                    isOffButtonShowing0 = true;
                    showConfirmButton();
                } else if (!isOffButtonShowing3 && isOffButtonShowing0 && isOffButtonShowing1 && isOffButtonShowing2) {
                    button3.setImageResource(R.drawable.offbutton);
                    isOffButtonShowing3 = true;
                    confirmButton.setVisibility(View.INVISIBLE);
                } else if (isOffButtonShowing3 && isOffButtonShowing0 && !isOffButtonShowing1 && isOffButtonShowing2) {
                    button3.setImageResource(R.drawable.onbutton);
                    isOffButtonShowing3 = false;
                    button1.setImageResource(R.drawable.offbutton);
                    isOffButtonShowing1 = true;
                    showConfirmButton();
                } else if (isOffButtonShowing3 && isOffButtonShowing0 && isOffButtonShowing1 && !isOffButtonShowing2) {
                    button3.setImageResource(R.drawable.onbutton);
                    isOffButtonShowing3 = false;
                    button2.setImageResource(R.drawable.offbutton);
                    isOffButtonShowing2 = true;
                    showConfirmButton();
                }
                else {
                    button0.setImageResource(R.drawable.offbutton);
                    button1.setImageResource(R.drawable.offbutton);
                    button2.setImageResource(R.drawable.offbutton);
                    button3.setImageResource(R.drawable.offbutton);
                    isOffButtonShowing0 = true;
                    isOffButtonShowing1 = true;
                    isOffButtonShowing2 = true;
                    isOffButtonShowing3 = true;
                    showConfirmButton();
                }

            }
        });

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hideOptionButtons();
                String name = optionName;
                crackTheEgg(name);

                Intent intent = getIntent();
                String friendPhoneNumber = intent.getStringExtra("friend_phone_number_from_list");
                String recipient_fcm_token = intent.getStringExtra("friend_fcm_token");
                String recipient_fcm_token_notification = intent.getStringExtra("recipient_fcm_token");

                mSharedPref = getSharedPreferences(Config.SHAREDPREFERENCES_MY_PHONE_NUMBER, Context.MODE_PRIVATE);
                String sender_phone_number = mSharedPref.getString(Config.MY_PHONE_NUMBER_KEY, "");

                mSharedPref = getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
                String myName = mSharedPref.getString(Config.SHAREDPREFERENCES_NAME, "");

                String question = listOfMessageFromFriend.get(0).getQuestion();
                String sender_fcm_token = FirebaseInstanceId.getInstance().getToken();

                if (recipient_fcm_token == null) {
                    UpdateMessageStatusTask updateMessageStatusTask = new UpdateMessageStatusTask();
                    updateMessageStatusTask.execute(friendPhoneNumber, myName, sender_phone_number, question, sender_fcm_token, recipient_fcm_token_notification, name);
                } else {
                    UpdateMessageStatusTask updateMessageStatusTask = new UpdateMessageStatusTask();
                    updateMessageStatusTask.execute(friendPhoneNumber, myName, sender_phone_number, question, sender_fcm_token, recipient_fcm_token, name);
                }

                 /*
                Media Player starts
                 */
//                mediaPlayer.start();
                Utility.onOffMediaPlayer(MessageReceivedActivity.this);

                /*
                Change the text in the confirm button to LOCKED
                 */
                confirmButton.setText("Locked!");
                confirmButton.setTextColor(Color.WHITE);
                confirmButton.setEnabled(false);
                YoYo.with(Techniques.SlideInRight).duration(200).playOn(confirmButton);
                YoYo.with(Techniques.Tada).delay(250).duration(400).playOn(confirmButton);

                /*
                Show the missed choses
                 */
                String[] myArraysOfOptions = new String[listOfMessageFromFriend.size()];

                for (int i=0; i<listOfMessageFromFriend.size(); i++) {

                    myArraysOfOptions[i] = listOfMessageFromFriend.get(i).getOption_one() + "\n" + listOfMessageFromFriend.get(i).getOption_two() + "\n" + listOfMessageFromFriend.get(i).getOption_three() + "\n" + listOfMessageFromFriend.get(i).getOption_four();
                }

                String[] mOptionArray = myArraysOfOptions[0].split("\n");

                List<String> mListOption = new ArrayList<String>();

                for (int j=0; j<mOptionArray.length; j++) {
                    mListOption.add(mOptionArray[j]);
                }

                for (int k=0; k<mListOption.size(); k++) {
                    if (mListOption.get(k).equals(optionName)) {
                        mListOption.remove(k);
                    }
                }

                showSnackBar("You missed: \n" +"1. " + mListOption.get(0) + " 2. " + mListOption.get(1) + " 3. " + mListOption.get(2));
            }
        });
    }

    public void makeWebserviceCallToGetMessageFromFriend(String friendPhoneNumber, String myPhoneNumber) {
        Request request = new Request.Builder()
                .url(Config.getMessageFromFriend(friendPhoneNumber, myPhoneNumber))
                .build();

        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {

                    String mResponse = response.body().string();

                    try {
                        JSONArray jsonArray = new JSONArray(mResponse);
                        for (int i=0; i<jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String question = jsonObject.getString("question");
                            String option_one = jsonObject.getString("option_one");
                            String option_two = jsonObject.getString("option_two");
                            String option_three = jsonObject.getString("option_three");
                            String option_four = jsonObject.getString("option_four");

                            MessageFromFriend messageFromFriend = new MessageFromFriend(question, option_one, option_two, option_three, option_four);
                            listOfMessageFromFriend.add(messageFromFriend);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            questionText.setText(listOfMessageFromFriend.get(0).getQuestion());
                            String optionOne = listOfMessageFromFriend.get(0).getOption_one();
                            String optionTwo = listOfMessageFromFriend.get(0).getOption_two();
                            String optionThree = listOfMessageFromFriend.get(0).getOption_three();
                            String optionFour = listOfMessageFromFriend.get(0).getOption_four();
                            selectTheOption(optionOne, optionTwo, optionThree, optionFour);
                        }
                    });

                }

            }
        });
    }

    public void updateTheMessageStatus(final String friendPhoneNumber, final String myName, final String sender_phone_number, String question, String sender_fcm_token, String recipient_fcm_token, String confirmed_option) {
        RequestBody requestBody = new FormBody.Builder()
                .add("sender_phone_number", friendPhoneNumber)
                .add("name_of_confirmer", myName)
                .add("receiver_phone_number", sender_phone_number)
                .add("question", question)
                .add("sender_fcm_token", sender_fcm_token)
                .add("recipient_fcm_token", recipient_fcm_token)
                .add("confirmed_option", confirmed_option)
                .build();

        Request request = new Request.Builder()
                .url(Config.URL_UPDATE_MESSAGE_STATUS)
                .post(requestBody)
                .build();

        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {

                    String mResponse = response.body().string();

                    try {
                        final JSONObject jsonObject = new JSONObject(mResponse);
                        if (jsonObject.names().get(0).equals("success")) {

                            /*
                            Navigate the user to the mainactivity after 5 section once the confirm button is clicked
                             */
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    Timer timer = new Timer();
                                    timer.schedule(new TimerTask() {
                                        @Override
                                        public void run() {
                                            navigateToMainActivity();
                                        }
                                    }, 5000);

                                }

                            });

                        } else {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    String error = null;
                                    try {
                                        error = jsonObject.getString("error");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                                }
                            });

                        }

                        response.body().close();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }
        });
    }

    class UpdateMessageStatusTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {

            updateTheMessageStatus(params[0], params[1], params[2], params[3], params[4], params[5], params[6]);

            return null;
        }
    }

    public void showSnackBar(String snackMessage) {
        snackbar.make(findViewById(android.R.id.content), snackMessage, Snackbar.LENGTH_LONG)
                .setDuration(6000)
                .setActionTextColor(Color.WHITE)
                .show();
    }
}
