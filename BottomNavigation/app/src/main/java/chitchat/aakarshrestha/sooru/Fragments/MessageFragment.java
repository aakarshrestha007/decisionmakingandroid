package chitchat.aakarshrestha.sooru.Fragments;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import chitchat.aakarshrestha.sooru.Adapter.FriendListAdapter;
import chitchat.aakarshrestha.sooru.Helper.Config;
import chitchat.aakarshrestha.sooru.Helper.FriendContact;
import chitchat.aakarshrestha.sooru.Helper.Utility;
import chitchat.aakarshrestha.bottomnavigation.R;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MessageFragment extends Fragment {

    protected RecyclerView recyclerView;
    protected LinearLayoutManager linearLayoutManager;

    private FriendListAdapter friendListAdapter;
    private List<FriendContact> friendContactList = new ArrayList<>();

    private OkHttpClient okHttpClient = new OkHttpClient();

    protected SharedPreferences mSharedPrefMyNumber;

    public static MessageFragment messageFragment;

    public ConstraintLayout messageFragmentLayout;

    private ProgressBar progressBar;
    private IntentFilter fcmIntentFilter;

    BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getRecyclerView();
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.message_fragment, container, false);

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        /*
        Initialize the recyclerView and linearLayoutManager
         */
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_friend_list_ID);
        linearLayoutManager = new LinearLayoutManager(getContext());
        messageFragment = this;

        messageFragmentLayout = (ConstraintLayout) view.findViewById(R.id.message_fragment_background);
        Utility.changeBackgroundColor(messageFragmentLayout, getContext());

        /*
        Add the action sent from the broadcast from MyFirebaseMessagingService file.
         */
        fcmIntentFilter = new IntentFilter();
        fcmIntentFilter.addAction(Config.FCM_ASK_RECEIVED_ACTION);

        /*
        Register the receiver
         */
        getActivity().registerReceiver(mBroadcastReceiver, fcmIntentFilter);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Timer timer = new Timer();

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        String permission = "android.permission.READ_CONTACTS";
                        int res = getContext().checkCallingPermission(permission);

                        if (res == PackageManager.PERMISSION_DENIED) {
                            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, 2);
                        }

//                        if (res == PackageManager.PERMISSION_GRANTED) {
//                            getMessageFragmentTasks();
//                        }
                    }
                });
            }
        };
        timer.schedule(timerTask, 100);
    }

    public void getMessageFragmentTasks() {
        mSharedPrefMyNumber = getActivity().getSharedPreferences(Config.SHAREDPREFERENCES_MY_PHONE_NUMBER, Context.MODE_PRIVATE);
        String my_phone_number = mSharedPrefMyNumber.getString(Config.MY_PHONE_NUMBER_KEY, "");

        MessageFragmentTask messageFragmentTask = new MessageFragmentTask();
        messageFragmentTask.execute(my_phone_number);

        MessageFragmentTaskNew messageFragmentTaskNew = new MessageFragmentTaskNew();
        messageFragmentTaskNew.execute(my_phone_number);
    }

    @Override
    public void onResume() {
        super.onResume();
        /*
        Register the receiver
         */
        getActivity().registerReceiver(mBroadcastReceiver, fcmIntentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 2) {

            if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)){
                getMessageFragmentTasks();
            }

        }

    }

    public void makeMessageFragmentWebServiceCall(final String my_phone_number) {

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.VISIBLE);
            }
        });

        Request request = new Request.Builder()
                .url(Config.retriveFriendSender(my_phone_number))
                .build();

        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                        }
                    });

                    String mResponse = response.body().string();

                    try {

                        JSONArray jsonArray = new JSONArray(mResponse);

                        for (int i=0; i<jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
//                            String user_one_name = jsonObject.getString("user_one_name");
//                            String user_one_phone = jsonObject.getString("user_one_phone");
                            String user_two_name = jsonObject.getString("user_two_name");
                            String user_two_phone = jsonObject.getString("user_two_phone");
                            String user_two_fcm_token = jsonObject.getString("user_two_fcm_token");

                            FriendContact friendContact = new FriendContact();
                            friendContact.setFriendName(user_two_name);
                            friendContact.setFriendPhoneNumber(user_two_phone);
                            friendContact.setFriendFcmToken(user_two_fcm_token);
                            friendContactList.add(friendContact);

                        }

                        getRecyclerView();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        });


    }

    public void getRecyclerView() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
             /*
                Sort the FriendContactListSender list
             */
                Collections.sort(friendContactList, new MyFriendListComp());

                friendListAdapter = new FriendListAdapter(getContext().getApplicationContext(), friendContactList);

                                /*
                                    Initialize the recyclerView and linearLayoutManager
                                */
                recyclerView = (RecyclerView) getActivity().findViewById(R.id.recycler_friend_list_ID);
                linearLayoutManager = new LinearLayoutManager(getContext());

                recyclerView.setAdapter(friendListAdapter);
                recyclerView.setLayoutManager(linearLayoutManager);
                friendListAdapter.notifyDataSetChanged();

            }
        });
    }

    public class MessageFragmentTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {

            makeMessageFragmentWebServiceCall(params[0]);
            return null;
        }

    }

    public void makeMessageFragmentWebServiceCallNew(final String my_phone_number) {

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.VISIBLE);
            }
        });

        Request request = new Request.Builder()
                .url(Config.retriveFriendReceiver(my_phone_number))
                .build();

        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                        }
                    });

                    String mResponse = response.body().string();

                    try {

                        JSONArray jsonArray = new JSONArray(mResponse);

                        for (int i=0; i<jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String user_one_name = jsonObject.getString("user_one_name");
                            String user_one_phone = jsonObject.getString("user_one_phone");
                            String user_one_fcm_token = jsonObject.getString("user_one_fcm_token");
//                            String user_two_name = jsonObject.getString("user_two_name");
//                            String user_two_phone = jsonObject.getString("user_two_phone");

                            FriendContact friendContact = new FriendContact();
                            friendContact.setFriendName(user_one_name);
                            friendContact.setFriendPhoneNumber(user_one_phone);
                            friendContact.setFriendFcmToken(user_one_fcm_token);
                            friendContactList.add(friendContact);

                        }

                        getRecyclerView();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        });


    }

    public class MessageFragmentTaskNew extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {

            makeMessageFragmentWebServiceCallNew(params[0]);
            return null;
        }

    }

    private class MyFriendListComp implements Comparator<FriendContact> {

        @Override
        public int compare(FriendContact o1, FriendContact o2) {
            return o1.getFriendName().compareTo(o2.getFriendName());
        }
    }
}
