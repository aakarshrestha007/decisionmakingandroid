package chitchat.aakarshrestha.sooru.Fragments;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import chitchat.aakarshrestha.sooru.Helper.Utility;
import chitchat.aakarshrestha.bottomnavigation.R;
import de.hdodenhof.circleimageview.CircleImageView;

public class ContactDetailFragment extends Fragment{

    private TextView textViewContactName, textViewContactNumber;
    private Button inviteButton;
    private ImageView backButtonImage;
    private CircleImageView friendCircleImage;
    private ConstraintLayout contactDetailFragmentLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.contact_detail_fragment, container, false);

        textViewContactName = (TextView) view.findViewById(R.id.friendContactsNameID);
        textViewContactNumber = (TextView) view.findViewById(R.id.phoneNumberID);
        inviteButton = (Button) view.findViewById(R.id.friendContactButtonID);
        friendCircleImage = (CircleImageView) view.findViewById(R.id.friendProfileimageID);
        backButtonImage = (ImageView) view.findViewById(R.id.contactImageViewBackButtonID);
        contactDetailFragmentLayout = (ConstraintLayout) view.findViewById(R.id.contactDetailFragment_ID);

        Utility.changeBackgroundColor(contactDetailFragmentLayout, getContext());
        Utility.changeTextColor(textViewContactNumber, getContext());

        final String contactName = getArguments().getString("contactname");
        final String contactNumber = getArguments().getString("contactPhoneNumber");
        final String contactPhoto = getArguments().getString("contactImage");
        final int itemPosition = getArguments().getInt("pos");

        backButtonImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FriendFragment friendFragment = new FriendFragment();
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right).replace(R.id.contentContainer, friendFragment).commit();

                Bundle bundle = new Bundle();
                bundle.putInt("positionOfItem", itemPosition);
                friendFragment.setArguments(bundle);

            }
        });

//        Log.v("ContactName", contactName);
//        Log.v("ContactNumber", contactNumber);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textViewContactName.setText(contactName);
                textViewContactNumber.setText(contactNumber);

                if (contactPhoto == null) {
                    Picasso.with(getContext()).load(R.drawable.dummy).into(friendCircleImage);
                } else {
                    Picasso.with(getContext()).load(contactPhoto).into(friendCircleImage);
                }

            }
        });

        inviteButton.setTextColor(Color.WHITE);
        inviteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ContactDetailFragment.this.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
                        View mView = layoutInflater.inflate(R.layout.invitation_alert_box, null);
                        TextView appNameText = (TextView) mView.findViewById(R.id.appNameID);
                        TextView textMsg = (TextView) mView.findViewById(R.id.text_instruction_ID);
                        Button noButton = (Button) mView.findViewById(R.id.invitation_no_id);
                        Button yesButton = (Button) mView.findViewById(R.id.invitation_yes_id);
                        builder.setView(mView);

                        appNameText.setText(getResources().getString(R.string.app_name));
                        textMsg.setText("Allow to access your SMS app to send invitation?");

                        final AlertDialog dialog = builder.create();

                        noButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        yesButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intentsms = new Intent( Intent.ACTION_VIEW, Uri.parse( "sms:" + "" ) );
                                intentsms.setType("vnd.android-dir/mms-sms");
                                intentsms.putExtra("address", contactNumber);
                                intentsms.putExtra("sms_body", "Join me on Sooru, a free and fun app! - https://play.google.com/store/apps/details?id=chitchat.aakarshrestha.sooru");
                                startActivity( intentsms );

                                dialog.dismiss();
                            }
                        });

                        dialog.show();

                    }
                });

            }
        });

        return view;

    }
}
