package chitchat.aakarshrestha.sooru.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

import chitchat.aakarshrestha.sooru.Helper.Config;
import chitchat.aakarshrestha.bottomnavigation.R;

public class IntroToApp extends AppIntro {

    private SharedPreferences introAppSharedPref;
    private SharedPreferences.Editor introAppEditor;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        introAppSharedPref = getSharedPreferences(Config.SHAREDPREFERENCES_INTRO_TO_APP, Context.MODE_PRIVATE);
        introAppEditor = introAppSharedPref.edit();

        addSlide(AppIntroFragment.newInstance("", "", R.drawable.welcome_slideicon, Color.parseColor("#00BBD3")));
        addSlide(AppIntroFragment.newInstance("Tap on friend's name", "Connecting to friends has never been this easy!", R.drawable.contacts_slideicon, Color.parseColor("#00BBD3")));
        addSlide(AppIntroFragment.newInstance("Tap on Ask to ask questions to friend", "You've got message when green dot is shown", R.drawable.friend_slideicon, Color.parseColor("#00BBD3")));
        addSlide(AppIntroFragment.newInstance("Your sent and received notifications", "Click on the Sent or Received button to view notifications", R.drawable.notif_slideicon, Color.parseColor("#00BBD3")));
        addSlide(AppIntroFragment.newInstance("Check out your settings", "Turn them ON or OFF as per your wish", R.drawable.settings_slideicon, Color.parseColor("#00BBD3")));
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);

        introAppEditor.putString(Config.INTRO_TO_APP_KEY_VALUE, "yes");
        introAppEditor.apply();

        finish();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);

        introAppEditor.putString(Config.INTRO_TO_APP_KEY_VALUE, "yes");
        introAppEditor.apply();

        finish();
    }
}
