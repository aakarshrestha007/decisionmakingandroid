package chitchat.aakarshrestha.sooru.Adapter;


import android.Manifest;
import android.app.NotificationManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import chitchat.aakarshrestha.sooru.Fragments.MessageFragment;
import chitchat.aakarshrestha.sooru.Helper.Config;
import chitchat.aakarshrestha.sooru.Helper.DataBaseHelper;
import chitchat.aakarshrestha.sooru.Helper.FriendContact;
import chitchat.aakarshrestha.sooru.Helper.Utility;
import chitchat.aakarshrestha.bottomnavigation.R;
import chitchat.aakarshrestha.sooru.UI.MainActivity;
import chitchat.aakarshrestha.sooru.UI.MessageReceivedActivity;
import chitchat.aakarshrestha.sooru.UI.MessageRoomActivity;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class FriendListAdapter extends RecyclerView.Adapter<FriendListAdapter.ViewHolder> {

    private Context mContext;
    private List<FriendContact> listOfFriends = new ArrayList<>();
    List<FriendContact> listOfFriendContacts;
    List<FriendContact> listOfUndecidedMessage;

    private OkHttpClient okHttpClient = new OkHttpClient();
    private SharedPreferences mSharedPref;

    DataBaseHelper dataBaseHelper;

    public FriendListAdapter(Context mContext, List<FriendContact> listOfFriends) {
        this.mContext = mContext;
        this.listOfFriends = listOfFriends;
    }

    private Uri contactUri;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.friendlist_item, parent, false);

        return new ViewHolder(itemView);
    }

//    @Override
//    public int getItemViewType(int position) {
//
//        mSharedPref = mContext.getSharedPreferences(Config.SHAREDPREFERENCES_MY_PHONE_NUMBER, Context.MODE_PRIVATE);
//        final String myPhoneNumber = mSharedPref.getString(Config.MY_PHONE_NUMBER_KEY, "");
//        final String friendPhoneNumber = listOfFriends.get(position).getFriendPhoneNumber();
//
//        listOfUndecidedMessage = new ArrayList<>();
//        newMessageGreenDot(myPhoneNumber, friendPhoneNumber, mContext);
//
//
//        int val = 0;
//        if (listOfUndecidedMessage != null) {
//
//            if (listOfUndecidedMessage.size() > 0) {
//
//                for (int i=0; i<listOfFriends.size(); i++) {
//                    String phoneOne = listOfFriends.get(i).getFriendPhoneNumber();
//
//                    for (int j=0; j<listOfUndecidedMessage.size(); j++) {
//                        String phoneAgain = listOfUndecidedMessage.get(j).getFriendPhoneNumber();
//
//                        if (phoneOne.equals(phoneAgain)) {
//                            val = 1;
//                        }
//                    }
//                }
//
//            }
//        }
//
//        return val;
//    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final String friendName = listOfFriends.get(position).getFriendName();
        final String friendPhoneNumber = listOfFriends.get(position).getFriendPhoneNumber();
        final String fcm_token = listOfFriends.get(position).getFriendFcmToken();

        mSharedPref = mContext.getSharedPreferences(Config.SHAREDPREFERENCES_MY_PHONE_NUMBER, Context.MODE_PRIVATE);
        final String myPhoneNumber = mSharedPref.getString(Config.MY_PHONE_NUMBER_KEY, "");

        listOfUndecidedMessage = new ArrayList<>();
        newMessageGreenDot(myPhoneNumber, friendPhoneNumber, mContext, holder);

         /*
        Set friend name in the list
         */
        holder.friendNameText.setText(friendName);
        Utility.changeTextColor(holder.friendNameText, holder.friendNameText.getContext());

        /*
            Pull the friend image from the phone.
         */
        listOfFriendContacts = new ArrayList<>();
        final Context context = MessageFragment.messageFragment.getContext();
        ContentResolver contentResolver = context.getContentResolver();

        contactUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.PHOTO_URI};

        Cursor phone = contentResolver.query(contactUri, projection, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");

        assert phone != null;
        while (phone.moveToNext()) {

            FriendContact friendContact = new FriendContact();

            String imageOfFriend = phone.getString(phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
            String phoneNumberOfFriend = phone.getString(phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            friendContact.setFriendImage(imageOfFriend);
            friendContact.setFriendPhoneNumber(phoneNumberOfFriend.replaceAll("\\D+", ""));

            listOfFriendContacts.add(friendContact);
        }

        phone.close();

        /*
            Show the image of the friend if the phone number matches
         */

        for (int i = 0; i < listOfFriendContacts.size(); i++) {
            FriendContact friendContact = listOfFriendContacts.get(i);

            if (friendPhoneNumber.equals(friendContact.getFriendPhoneNumber())) {

                if (friendContact.getFriendImage() == null) {
                    Picasso.with(context).load(R.drawable.dummy).into(holder.friendCircleImageView);
                } else {
                    Picasso.with(context).load(friendContact.getFriendImage()).into(holder.friendCircleImageView);
                }
            }

        }

        /*
         Set notification status icon
         */

//        listOfUndecidedMessage = new ArrayList<>();
//        newMessageGreenDot(myPhoneNumber, friendPhoneNumber, mContext, holder);

        holder.friendNameText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*
                    Delete the Sqlite DB if the value matches and cancel all the notification(s) from
                    the status bar
                 */
                dataBaseHelper = new DataBaseHelper(mContext);
                dataBaseHelper.deleteAskNotificationTable();
                dataBaseHelper.deleteAnswerNotificationTable();
                dataBaseHelper.deleteTextMessageNotification();

                NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.cancelAll();

                if (Utility.isConnectedToNetwork(MessageFragment.messageFragment.getContext())) {
                    Request request = new Request.Builder()
                            .url(Config.checkMessageFromFriend(friendPhoneNumber, myPhoneNumber))
                            .build();

                    Call call = okHttpClient.newCall(request);
                    call.enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {

                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {

                            if (response.isSuccessful()) {
                                String mResponse = response.body().string();
                                try {
                                    JSONObject jsonObject = new JSONObject(mResponse);

                                    if (jsonObject.names().get(0).equals("success")) {
                                        MessageFragment.messageFragment.getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                for (int i = 0; i < listOfFriendContacts.size(); i++) {

                                                    FriendContact frenContact = listOfFriendContacts.get(i);

                                                    if (friendPhoneNumber.equals(frenContact.getFriendPhoneNumber())) {

                                                        if (frenContact.getFriendImage() != null) {
                                                            navigateToMessageReceivedActivity(friendName, friendPhoneNumber, fcm_token, frenContact.getFriendImage());
                                                        } else {
                                                            navigateToMessageReceivedActivity(friendName, friendPhoneNumber, fcm_token, null);
                                                        }

                                                    }

                                                }

                                                //navigateToMessageReceivedActivity(friendName, friendPhoneNumber, fcm_token);
                                            }
                                        });
                                    } else {
                                        MessageFragment.messageFragment.getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(MessageFragment.messageFragment.getActivity(), "No message received from " + friendName + "!", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                        }
                    });
                } else {
                    Utility.showNetworkConnectionAlertMessage(MessageFragment.messageFragment.getContext(), "Network Connectivity", "Please connect to the network.");
                }

            }
        });

        holder.askButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Utility.isConnectedToNetwork(MessageFragment.messageFragment.getContext())) {

                    //Navigate to MessageRoomActivity activity.
                    for (int i = 0; i < listOfFriendContacts.size(); i++) {

                        FriendContact fContact = listOfFriendContacts.get(i);

                        if (friendPhoneNumber.equals(fContact.getFriendPhoneNumber())) {

                            if (fContact.getFriendImage() != null) {
                                navigateToMessageRoomActivity(friendName, friendPhoneNumber, fcm_token, fContact.getFriendImage());
                            } else {
                                navigateToMessageRoomActivity(friendName, friendPhoneNumber, fcm_token, null);
                            }

                        }

                    }
                } else {
                    Utility.showNetworkConnectionAlertMessage(MessageFragment.messageFragment.getContext(), "Network Connectivity", "Please connect to the network.");
                }

            }
        });
    }

    private void newMessageGreenDot(String myPhoneNumber, final String friendPhoneNumber, final Context context, final ViewHolder holder) {

        Request request = new Request.Builder()
                .url(Config.retrieveUnDecidedMessages(myPhoneNumber))
                .build();

        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {

                    String mResponse = response.body().string();
                    try {
                        JSONArray jsonArray = new JSONArray(mResponse);

                        FriendContact friendContactNow = new FriendContact();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String user_one_phone = jsonObject.getString("user_one_phone");
                            String sender_name = jsonObject.getString("sender_name");

                            friendContactNow.setFriendPhoneNumber(user_one_phone);
                            friendContactNow.setFriendName(sender_name);

                            listOfUndecidedMessage.add(friendContactNow);
                        }

                        for (int i = 0; i < listOfUndecidedMessage.size(); i++) {
                            FriendContact friendContact = listOfUndecidedMessage.get(i);

                            if (friendPhoneNumber.equals(friendContact.getFriendPhoneNumber())) {

                                MessageFragment.messageFragment.getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Picasso.with(context).load(R.drawable.notificationstatusdot).into(holder.notificationImageIconView);
                                    }
                                });

                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }


            }
        });

    }

    @Override
    public int getItemCount() {
        return listOfFriends.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView friendNameText;
        Button askButton;
        CircleImageView friendCircleImageView, notificationImageIconView;

        ViewHolder(View itemView) {
            super(itemView);

            friendNameText = (TextView) itemView.findViewById(R.id.friendNameFromListID);
            askButton = (Button) itemView.findViewById(R.id.askButtonID);
            friendCircleImageView = (CircleImageView) itemView.findViewById(R.id.friendlist_item_image_ID);
            notificationImageIconView = (CircleImageView) itemView.findViewById(R.id.notificationDotID);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private void navigateToMessageReceivedActivity(String friendName, String friendPhoneNumber, String friend_fcm_token, String image_of_friend) {
        Intent intent = new Intent(mContext, MessageReceivedActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("friend_name_from_list", friendName);
        intent.putExtra("friend_phone_number_from_list", friendPhoneNumber);
        intent.putExtra("friend_fcm_token", friend_fcm_token);
        intent.putExtra("image_of_friend", image_of_friend);
        intent.putExtra("askNotificationKey", "1");
        mContext.startActivity(intent);

        MessageFragment.messageFragment.getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }

    private void navigateToMessageRoomActivity(String friendName, String friendPhoneNumber, String fcm_token, String friend_image) {
        Intent intent = new Intent(mContext, MessageRoomActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("contactname", friendName);
        intent.putExtra("contactPhoneNumber", friendPhoneNumber);
        intent.putExtra("recipient_fcm_token", fcm_token);
        intent.putExtra("friend_image", friend_image);
        mContext.startActivity(intent);

        MessageFragment.messageFragment.getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }
}