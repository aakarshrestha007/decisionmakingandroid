package chitchat.aakarshrestha.sooru.Helper;

public class Config {

    static final String host = "192.168.1.70" /*"99.70.246.172"*/;
    static final String port = "8888"/*"80"*/;
    static final String folderName = "decisionMaker";

    public static final String FACEBOOK_NAME = "facebookName";
    public static final String FACEBOOK_ID = "id";
    public static final String FACEBOOK_PROFILE_IMAGE = "facebookProfileImage";

    public static final String SHAREDPREFERENCES = "fbSharedPref";
    public static final String SHAREDPREFERENCES_NAME = "sharedPrefName";
    public static final String SHAREDPREFERENCES_ID = "sharedPrefID";
    public static final String SHAREDPREFERENCES_IMAGE = "sharePrefImage";
    public static final String SHAREDPREFERENCES_MY_PHONE_NUMBER = "SharedPrefMyPhoneNumber";
    public static final String SHAREDPREFERENCES_NIGHT_MODE = "NightMode";
    public static final String SHAREDPREFERENCES_NOTIFICATION_MODE = "NotificationMode";
    public static final String SHAREDPREFERENCES_SOUND_EFFECT_MODE = "SoundEffect";
    public static final String SHAREDPREFERENCES_INTRO_TO_APP = "Intro SharedPref";

    public static final String MY_PHONE_NUMBER_KEY = "myPhoneNumber";
    public static final String NIGHT_MODE_KEY_VALUE = "nightModeOn";
    public static final String NOTIFICATION_MODE_KEY_VALUE = "notificationModeOn";
    public static final String SOUND_EFFECT_MODE_KEY_VALUE = "soundEffectModeOn";
    public static final String INTRO_TO_APP_KEY_VALUE = "introAppKey";

    public static final String URL_BASIC = "http://"+host+":"+port+"/"+folderName+"/";
    public static final String URL_USERS = URL_BASIC + "users.php";
    public static final String URL_USER_LOGOUT = URL_BASIC + "UpdateFCMRegID.php";
    public static final String URL_FRIEND_EXISTENCE = URL_BASIC + "friendExistence.php?phonenumber=";
    public static final String URL_FRIEND_CONNECTION = URL_BASIC + "FriendConnection.php";
    public static final String URL_LIST_OF_FRIENDS = URL_BASIC + "listOfFriends.php";
    public static final String URL_UPDATE_MESSAGE_STATUS = URL_BASIC + "updateMessageStatus.php";
    public static final String URL_PROFILE_MESSAGE_LIST = URL_BASIC + "messageListInProfile.php?sender_phoneNumber=";
    public static final String URL_PROFILE_MESSAGE_LIST_ONE = URL_BASIC + "messageListInProfileOne.php?sender_phoneNumber=";
    public static final String URL_TEXT_MESSAGE_SEND = URL_BASIC + "TextMessageSend.php";
    public static final String URL_TYPING_STATUS = URL_BASIC + "ShowTypingStatus.php";

    public static final String FCM_ASK_RECEIVED_ACTION = "FCM_ASK_RECEIVED_ACTION";
    public static final String FCM_CONFIRM_RECEIVED_ACTION = "FCM_CONFIRM_RECEIVED_ACTION";
    public static final String FCM_TEXT_MESSAGE_RECEIVED_ACTION = "FCM_TEXT_MESSAGE_RECEIVED_ACTION";

    public static String urlToGetTextMessages(String sender_phone, String receiver_phone, int limitNum) {
        return URL_BASIC + "GetTextMessage.php?sender_phone=" + sender_phone + "&receiver_phone="+receiver_phone+"&limitNumber="+limitNum;
    }

    public static String getMessageFromFriend(String sender_phonenumber, String receiver_phonenumber) {
        return URL_BASIC + "MessageFromFriend.php?sender_phonenumber=" + sender_phonenumber + "&receiver_phonenumber=" + receiver_phonenumber;
    }

    public static String checkMessageFromFriend(String sender_phonenumber, String receiver_phonenumber) {
        return URL_BASIC + "CheckMessageFromFriend.php?sender_phonenumber=" + sender_phonenumber + "&receiver_phonenumber=" + receiver_phonenumber;
    }

    public static String retriveFriendSender(String userphone) {
        return URL_BASIC + "retriveFriendSender.php?user_phone="+userphone;
    }

    public static String retriveFriendReceiver(String userphone) {
        return URL_BASIC + "retriveFriendReceiver.php?user_phone="+userphone;
    }

    public static String retrieveUnDecidedMessages(String my_phone_number) {
        return URL_BASIC + "UnDecidedMessages.php?receiver_phone_number=" + my_phone_number;
    }

    public static String showSentQuestionAnswer(String sender_phoneNumber, String receiver_phoneNumber, String question) {
        return URL_BASIC + "ShowSentQuestionAnswer.php?sender_phoneNumber="+sender_phoneNumber+"&receiver_phoneNumber="+
                receiver_phoneNumber+"&question="+question;
    }

}
