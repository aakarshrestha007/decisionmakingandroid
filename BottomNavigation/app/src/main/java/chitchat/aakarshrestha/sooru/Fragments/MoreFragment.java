package chitchat.aakarshrestha.sooru.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import chitchat.aakarshrestha.sooru.Adapter.MoreFragmentAdapter;
import chitchat.aakarshrestha.sooru.Helper.Utility;
import chitchat.aakarshrestha.bottomnavigation.R;

public class MoreFragment extends Fragment{

    public ConstraintLayout moreFragmentLayout;

    public static MoreFragment mMoreFragment;

    public TextView settingText, addContactText, shareText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.more_fragment, container, false);

        mMoreFragment = this;

        /*
        Night mode
         */
        moreFragmentLayout = (ConstraintLayout) view.findViewById(R.id.moreFragmentLayoutID);
        Utility.changeBackgroundColor(moreFragmentLayout, getContext());

        settingText = (TextView) view.findViewById(R.id.moreFragmentRecyclerView_ID);
        addContactText = (TextView) view.findViewById(R.id.add_contacts_ID);
        shareText = (TextView) view.findViewById(R.id.shareID);

        Utility.changeTextColor(settingText, getContext());
        Utility.changeTextColor(addContactText, getContext());
        Utility.changeTextColor(shareText, getContext());

        settingText.setText("Settings");
        settingText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingFragment settingFragment = new SettingFragment();
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left).replace(R.id.contentContainer, settingFragment).commit();
            }
        });

        addContactText.setText("Add Contacts");
        addContactText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                Using the explicit intent couldn't be useful because it is import to have the same package/activity in all phones.
                 */
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.setData(Uri.parse("content://contacts/people/"));
                startActivity(i);
            }
        });

        shareText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, "Join me on Sooru, a free and fun app! - https://play.google.com/store/apps/details?id=chitchat.aakarshrestha.sooru");
                        sendIntent.setType("text/plain");
                        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.share_via)));
                    }
                });

            }
        });


        return view;
    }
}
