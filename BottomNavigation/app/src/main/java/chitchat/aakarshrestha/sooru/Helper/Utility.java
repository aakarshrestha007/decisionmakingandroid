package chitchat.aakarshrestha.sooru.Helper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import chitchat.aakarshrestha.bottomnavigation.R;
import chitchat.aakarshrestha.sooru.UI.MainActivity;

public class Utility {

    public static void changeBackgroundColor(View view, Context mContext) {

        SharedPreferences sharedPreferences = mContext.getSharedPreferences(Config.SHAREDPREFERENCES_NIGHT_MODE, mContext.MODE_PRIVATE);
        String nightModeValue = sharedPreferences.getString(Config.NIGHT_MODE_KEY_VALUE, "");

        if (nightModeValue.equals("Night Mode")) {
            Calendar calendar = Calendar.getInstance();

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
            String Date = simpleDateFormat.format(calendar.getTime());

            int sunrise = 19800; //5:30 am
            int sunset = 70200; //7:30 pm

            String hour, minute;
            String[] timeArray = Date.split(":");
            hour = timeArray[0];
            minute = timeArray[1];

            int hourInt = Integer.parseInt(hour);
            int minuteInt = Integer.parseInt(minute);

            int hourAndMinuteInSeconds = (hourInt * 3600) + (minuteInt * 60);

            if (hourAndMinuteInSeconds >= sunset  || hourAndMinuteInSeconds <= sunrise) {
                view.setBackgroundColor(Color.BLACK);
            } else {
                view.setBackgroundColor(Color.WHITE);
            }
        }


    }

    public static void changeColorOfEditTextField(View view, Context mContext) {

        SharedPreferences sharedPreferences = mContext.getSharedPreferences(Config.SHAREDPREFERENCES_NIGHT_MODE, mContext.MODE_PRIVATE);
        String nightModeValue = sharedPreferences.getString(Config.NIGHT_MODE_KEY_VALUE, "");

        if (nightModeValue.equals("Night Mode")) {
            Calendar calendar = Calendar.getInstance();

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
            String Date = simpleDateFormat.format(calendar.getTime());

            int sunrise = 19800; //5:30 am
            int sunset = 70200; //7:30 pm

            String hour, minute;
            String[] timeArray = Date.split(":");
            hour = timeArray[0];
            minute = timeArray[1];

            int hourInt = Integer.parseInt(hour);
            int minuteInt = Integer.parseInt(minute);

            int hourAndMinuteInSeconds = (hourInt * 3600) + (minuteInt * 60);

            if (hourAndMinuteInSeconds >= sunset  || hourAndMinuteInSeconds <= sunrise) {
                view.setBackgroundColor(Color.GRAY);
            }
        }

    }

    public static void changeTextColor(TextView textView, Context mContext) {

        SharedPreferences sharedPreferences = mContext.getSharedPreferences(Config.SHAREDPREFERENCES_NIGHT_MODE, mContext.MODE_PRIVATE);
        String nightModeValue = sharedPreferences.getString(Config.NIGHT_MODE_KEY_VALUE, "");

        if (nightModeValue.equals("Night Mode")) {
            Calendar calendar = Calendar.getInstance();

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
            String Date = simpleDateFormat.format(calendar.getTime());

            int sunrise = 19800; //5:30 am
            int sunset = 70200; //7:30 pm

            String hour, minute;
            String[] timeArray = Date.split(":");
            hour = timeArray[0];
            minute = timeArray[1];

            int hourInt = Integer.parseInt(hour);
            int minuteInt = Integer.parseInt(minute);

            int hourAndMinuteInSeconds = (hourInt * 3600) + (minuteInt * 60);

            if (hourAndMinuteInSeconds >= sunset  || hourAndMinuteInSeconds <= sunrise) {
                textView.setTextColor(textView.getResources().getColor(R.color.white));
            }
        }

    }

    public static void changeCardViewColor(CardView cardView, Context mContext) {

        SharedPreferences sharedPreferences = mContext.getSharedPreferences(Config.SHAREDPREFERENCES_NIGHT_MODE, mContext.MODE_PRIVATE);
        String nightModeValue = sharedPreferences.getString(Config.NIGHT_MODE_KEY_VALUE, "");

        if (nightModeValue.equals("Night Mode")) {
            Calendar calendar = Calendar.getInstance();

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
            String Date = simpleDateFormat.format(calendar.getTime());

            int sunrise = 19800; //5:30 am
            int sunset = 70200; //7:30 pm

            String hour, minute;
            String[] timeArray = Date.split(":");
            hour = timeArray[0];
            minute = timeArray[1];

            int hourInt = Integer.parseInt(hour);
            int minuteInt = Integer.parseInt(minute);

            int hourAndMinuteInSeconds = (hourInt * 3600) + (minuteInt * 60);

            if (hourAndMinuteInSeconds >= sunset  || hourAndMinuteInSeconds <= sunrise) {
                cardView.setBackgroundColor(cardView.getResources().getColor(R.color.lightBlue));
            }
        }

    }


    public static boolean isConnectedToNetwork(Context context) {

        boolean isConnected;

        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            isConnected = true;
        } else {
            isConnected = false;
        }

        return isConnected;
    }

    public static void showNetworkConnectionAlertMessage(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setIcon(R.drawable.error_icon)
                .setPositiveButton("OK", null);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public static void autoLogin(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
        String user_name = sharedPreferences.getString(Config.SHAREDPREFERENCES_NAME, "");

        if (user_name.length() > 0) {
            Intent autologinIntent = new Intent(context, MainActivity.class);
            autologinIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(autologinIntent);
        }
    }

    public static String niceDateFormatter(String date) {

        String dateValue = date;

        String[] splitDatevalue = dateValue.split(" ");

        String[] yearMonthDayArray = splitDatevalue[0].split("-");
        String yearValue = yearMonthDayArray[0];
        String monthValue = yearMonthDayArray[1];
        String dayValue = yearMonthDayArray[2];

        switch (monthValue) {

            case "01":
                dateValue = "Jan";
                break;
            case "02":
                dateValue = "Feb";
                break;
            case "03":
                dateValue = "Mar";
                break;
            case "04":
                dateValue = "Apr";
                break;
            case "05":
                dateValue = "May";
                break;
            case "06":
                dateValue = "Jun";
                break;
            case "07":
                dateValue = "Jul";
                break;
            case "08":
                dateValue = "Aug";
                break;
            case "09":
                dateValue = "Sept";
                break;
            case "10":
                dateValue = "Oct";
                break;
            case "11":
                dateValue = "Nov";
                break;
            case "12":
                dateValue = "Dec";
                break;
        }

        return dayValue + " " + dateValue + " '" + yearValue.charAt(2) + yearValue.charAt(3);

    }

    public static void onOffMediaPlayer(Context context) {
        MediaPlayer mediaPlayer = MediaPlayer.create(context, R.raw.sound_effect);

        SharedPreferences sharedPreferences = context.getSharedPreferences(Config.SHAREDPREFERENCES_SOUND_EFFECT_MODE, context.MODE_PRIVATE);
        String soundEffectModeValue = sharedPreferences.getString(Config.SOUND_EFFECT_MODE_KEY_VALUE, "");

        if (soundEffectModeValue.equals("Sound Effect Mode")) {
            mediaPlayer.start();
        }

    }

}
