package chitchat.aakarshrestha.bottomnavigation.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import chitchat.aakarshrestha.bottomnavigation.Helper.Config;
import chitchat.aakarshrestha.bottomnavigation.Helper.Utility;
import chitchat.aakarshrestha.bottomnavigation.R;

public class SettingFragmentAdapter extends RecyclerView.Adapter<SettingFragmentAdapter.ViewHolder>{

    Context mContext;
    List<String> listOfSettingItems = new ArrayList<>();

    public SettingFragmentAdapter(Context mContext, List<String> listOfSettingItems) {
        this.mContext = mContext;
        this.listOfSettingItems = listOfSettingItems;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.setting_fragment_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.onOffTextItem.setText(listOfSettingItems.get(position));

        SharedPreferences sharedPreferencesNightMode = mContext.getSharedPreferences(Config.SHAREDPREFERENCES_NIGHT_MODE, mContext.MODE_PRIVATE);
        final SharedPreferences.Editor nightModeEditor = sharedPreferencesNightMode.edit();

        holder.aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (holder.aSwitch.isChecked()) {
//                    Log.v("isChecked", listOfSettingItems.get(position));
                    nightModeEditor.putString(Config.NIGHT_MODE_KEY_VALUE, listOfSettingItems.get(position));
                    nightModeEditor.apply();

                } else {
                    nightModeEditor.remove(Config.NIGHT_MODE_KEY_VALUE);
                    nightModeEditor.apply();
                }

            }
        });


        String nightModeStatus = sharedPreferencesNightMode.getString(Config.NIGHT_MODE_KEY_VALUE, "");
        if (!nightModeStatus.equals("")) {
            holder.aSwitch.setChecked(true);
        } else {
            holder.aSwitch.setChecked(false);
        }

        Utility.changeTextColor(holder.onOffTextItem, holder.onOffTextItem.getContext());

    }

    @Override
    public int getItemCount() {
        return listOfSettingItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView onOffTextItem;
        Switch aSwitch;

        public ViewHolder(View itemView) {
            super(itemView);

            onOffTextItem = (TextView) itemView.findViewById(R.id.onOffSwitchItemID);
            aSwitch = (Switch) itemView.findViewById(R.id.onOffSwitchID);
        }
    }
}
