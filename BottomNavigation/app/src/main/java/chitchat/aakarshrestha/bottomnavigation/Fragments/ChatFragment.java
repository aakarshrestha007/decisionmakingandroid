package chitchat.aakarshrestha.bottomnavigation.Fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import chitchat.aakarshrestha.bottomnavigation.R;
import chitchat.aakarshrestha.bottomnavigation.UI.MessageRoomActivity;

public class ChatFragment extends Fragment {

    private TextView username_chatTextView;
    private ImageButton chat_activity_backArrow;
    private RelativeLayout relativeLayout;
    private Button sendTextButton;
    private EditText messageEditText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.chat_fragment, container, false);

        username_chatTextView = (TextView) view.findViewById(R.id.username_chatTextViewID);
        chat_activity_backArrow = (ImageButton) view.findViewById(R.id.chat_activity_backArrow);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.chatRelativeLayoutID);
        sendTextButton = (Button) view.findViewById(R.id.buttonSendID);
        messageEditText = (EditText) view.findViewById(R.id.messageEditTextID);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final String chatUserNameValue = getArguments().getString("chat_username");
        final String chatContactPhoneNumberValue = getArguments().getString("chat_contactPhoneNumber");
        final String chatFriendImage = getArguments().getString("chat_friend_image");
        String chatRecipient_FcmTokenValue = getArguments().getString("chat_recipient_fcm_token");

        username_chatTextView.setText(chatUserNameValue);
        Picasso.with(getActivity().getApplicationContext()).load(chatFriendImage).into(new com.squareup.picasso.Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                relativeLayout.setBackground(new BitmapDrawable(getActivity().getApplicationContext().getResources(), bitmap));
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });

        username_chatTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getIntentValues(chatUserNameValue, chatContactPhoneNumberValue, chatFriendImage);
            }
        });

        chat_activity_backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getIntentValues(chatUserNameValue, chatContactPhoneNumberValue, chatFriendImage);
            }
        });


        sendTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String textMessage = messageEditText.getText().toString().trim();
                if (!textMessage.isEmpty()) {
                    Toast.makeText(getActivity().getApplicationContext(), textMessage, Toast.LENGTH_LONG).show();
                    messageEditText.setText("");
                }

            }
        });
    }

    private void getIntentValues(String chatUserNameValue, String chatContactPhoneNumberValue, String chatFriendImage) {

        Intent goBackIntent = new Intent(getActivity().getApplicationContext(), MessageRoomActivity.class);
        goBackIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        goBackIntent.putExtra("contactname", chatUserNameValue);
        goBackIntent.putExtra("contactPhoneNumber", chatContactPhoneNumberValue);
        goBackIntent.putExtra("friend_image", chatFriendImage);
        startActivity(goBackIntent);

        MessageFragment.messageFragment.getActivity().overridePendingTransition(R.anim.exit_to_right, R.anim.enter_from_left);
    }
}
