package chitchat.aakarshrestha.bottomnavigation.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import chitchat.aakarshrestha.bottomnavigation.Helper.Config;
import chitchat.aakarshrestha.bottomnavigation.Helper.Utility;
import chitchat.aakarshrestha.bottomnavigation.R;
import chitchat.aakarshrestha.bottomnavigation.UI.MainActivity;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SettingFragment extends Fragment{

    protected Button logOutButton;
    protected ImageView backButtonImage;

    public SharedPreferences mSharedPreferences, mSharedPreferencesPhoneNumber, chatIntroSharedPref;
    public SharedPreferences.Editor mEditor;

    public ConstraintLayout settingFragmentLayout;
    protected Switch nightModeSwitch, notificationSwitch, soundEffectSwitch;
    protected Snackbar snackbar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.setting_fragment, container, false);

        backButtonImage = (ImageView) view.findViewById(R.id.setting_back_button_ID);
        backButtonImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MoreFragment alertsFragment = new MoreFragment();
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right).replace(R.id.contentContainer, alertsFragment).commit();
            }
        });

        /*
        Night mode
         */
        settingFragmentLayout = (ConstraintLayout) view.findViewById(R.id.settingFragmentID);
        Utility.changeBackgroundColor(settingFragmentLayout, getContext());

        nightModeSwitch = (Switch) view.findViewById(R.id.nightModeOnOffSwitchID);
        notificationSwitch = (Switch) view.findViewById(R.id.notificationOnOffSwitchID);
        soundEffectSwitch = (Switch) view.findViewById(R.id.soundEffect_ID);

        SharedPreferences sharedPreferencesNightMode = getActivity().getSharedPreferences(Config.SHAREDPREFERENCES_NIGHT_MODE, Context.MODE_PRIVATE);
        final SharedPreferences.Editor nightModeEditor = sharedPreferencesNightMode.edit();

        /*
        Night Mode switch function
         */
        nightModeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (nightModeSwitch.isChecked()) {
                    nightModeEditor.putString(Config.NIGHT_MODE_KEY_VALUE, "Night Mode");
                    nightModeEditor.apply();

                } else {
                    nightModeEditor.remove(Config.NIGHT_MODE_KEY_VALUE);
                    nightModeEditor.apply();
                }

            }
        });

        String nightModeStatus = sharedPreferencesNightMode.getString(Config.NIGHT_MODE_KEY_VALUE, "");
        if (!nightModeStatus.equals("")) {
            nightModeSwitch.setChecked(true);
        } else {
            nightModeSwitch.setChecked(false);
        }

        Utility.changeTextColor(nightModeSwitch, getContext());

        /*
        Notification switch function
         */

        SharedPreferences sharedPreferencesNotificationMode = getActivity().getSharedPreferences(Config.SHAREDPREFERENCES_NOTIFICATION_MODE, Context.MODE_PRIVATE);
        final SharedPreferences.Editor notificationEditor = sharedPreferencesNotificationMode.edit();

        notificationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (notificationSwitch.isChecked()) {
                    notificationEditor.putString(Config.NOTIFICATION_MODE_KEY_VALUE, "Notification Mode");
                    notificationEditor.apply();
                } else {
                    notificationEditor.remove(Config.NOTIFICATION_MODE_KEY_VALUE);
                    notificationEditor.apply();
                }

            }
        });

        String notificationModeStatus = sharedPreferencesNotificationMode.getString(Config.NOTIFICATION_MODE_KEY_VALUE, "");
        if (!notificationModeStatus.equals("")) {
            notificationSwitch.setChecked(true);
        } else {
            notificationSwitch.setChecked(false);
        }

        Utility.changeTextColor(notificationSwitch, getContext());

        /*
            Sound Effect switch
         */
        SharedPreferences sharedPreferencesSoundEffectMode = getActivity().getSharedPreferences(Config.SHAREDPREFERENCES_SOUND_EFFECT_MODE, Context.MODE_PRIVATE);
        final SharedPreferences.Editor soundEffectEditor = sharedPreferencesSoundEffectMode.edit();

        soundEffectSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (soundEffectSwitch.isChecked()) {
                    soundEffectEditor.putString(Config.SOUND_EFFECT_MODE_KEY_VALUE, "Sound Effect Mode");
                    soundEffectEditor.apply();
                } else {
                    soundEffectEditor.remove(Config.SOUND_EFFECT_MODE_KEY_VALUE);
                    soundEffectEditor.apply();
                }

            }
        });

        String soundEffectModeStatus = sharedPreferencesSoundEffectMode.getString(Config.SOUND_EFFECT_MODE_KEY_VALUE, "");
        if (!soundEffectModeStatus.equals("")) {
            soundEffectSwitch.setChecked(true);
        } else {
            soundEffectSwitch.setChecked(false);
        }

        Utility.changeTextColor(soundEffectSwitch, getContext());


        /*
        Logout button function
         */
        logOutButton = (Button) view.findViewById(R.id.logoutID);
        logOutButton.setTextColor(Color.WHITE);

        logOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mSharedPreferences = getActivity().getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
                final String username = mSharedPreferences.getString(Config.SHAREDPREFERENCES_NAME,"");

                mSharedPreferencesPhoneNumber = getActivity().getSharedPreferences(Config.SHAREDPREFERENCES_MY_PHONE_NUMBER, Context.MODE_PRIVATE);
                final String myPhoneNumber = mSharedPreferencesPhoneNumber.getString(Config.MY_PHONE_NUMBER_KEY, "");

                final String appName = getContext().getResources().getString(R.string.app_name);

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                        View mView = layoutInflater.inflate(R.layout.logout_alert_box, null);
                        TextView logoutTitle = (TextView) mView.findViewById(R.id.logoutTextID);
                        TextView logoutQuestion = (TextView) mView.findViewById(R.id.logout_instruction_ID);
                        logoutTitle.setText("Log Out");
                        logoutQuestion.setText("Are you sure you want to log out from " + appName + "?");
                        Button logoutYes = (Button) mView.findViewById(R.id.logout_yes_id);
                        Button logoutNo = (Button) mView.findViewById(R.id.logout_no_id);
                        builder.setView(mView);

                        final AlertDialog dialog = builder.create();

                        logoutNo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        logoutYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                while (!removeUserInfoFromSharedPreference()) {
                                    removeUserInfoFromSharedPreference();
                                }

                                UpdateFCMidTask updateFCMidTask = new UpdateFCMidTask();
                                updateFCMidTask.execute(username, myPhoneNumber);

                                LoginManager.getInstance().logOut();
                                MainActivity.mMainActivity.goToLoginActivity();

                                dialog.dismiss();
                            }
                        });

                        dialog.show();
                    }
                });

                /*Clear the value to show chat alert*/
                chatIntroSharedPref = getActivity().getSharedPreferences("CHATSHAREDPREFERENCES", Context.MODE_PRIVATE);
                mEditor = chatIntroSharedPref.edit();
                mEditor.remove("okOnChatIntroSeen");
                mEditor.commit();
            }
        });

        return view;

    }


    public boolean removeUserInfoFromSharedPreference() {
        boolean isUserInfoRemoved = false;

        mSharedPreferences = getActivity().getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
        mEditor.remove(Config.SHAREDPREFERENCES_NAME);
        mEditor.remove(Config.SHAREDPREFERENCES_ID);
        mEditor.remove(Config.SHAREDPREFERENCES_IMAGE);
        mEditor.apply();

        mSharedPreferences = getActivity().getSharedPreferences(Config.SHAREDPREFERENCES_INTRO_TO_APP, Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
        mEditor.remove(Config.INTRO_TO_APP_KEY_VALUE);
        mEditor.apply();

        if ((mSharedPreferences.getString(Config.SHAREDPREFERENCES_NAME, "").length() == 0) && (mSharedPreferences.getString(Config.SHAREDPREFERENCES_ID,"").length() == 0) &&
                (mSharedPreferences.getString(Config.SHAREDPREFERENCES_IMAGE,"").length() == 0) && (mSharedPreferences.getString(Config.INTRO_TO_APP_KEY_VALUE, "").length() == 0)) {
            isUserInfoRemoved = true;
        }

        return isUserInfoRemoved;
    }

    public void updateFCMRegIDAtLogout(String username, String phonenumber) {

        OkHttpClient okHttpClient = new OkHttpClient();
        RequestBody requestBody = new FormBody.Builder()
                .add("username", username)
                .add("phonenumber", phonenumber)
                .build();

        Request request = new Request.Builder()
                .url(Config.URL_USER_LOGOUT)
                .post(requestBody)
                .build();

        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {

                    try {
                        String mResponse = response.body().string();
                        JSONObject jsonObject = new JSONObject(mResponse);

                        if (jsonObject.names().get(0).equals("success")) {

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //Toast.makeText(getContext(), "We will miss you!", Toast.LENGTH_SHORT).show();
                                }
                            });

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }
        });

    }

    private class UpdateFCMidTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {

            updateFCMRegIDAtLogout(params[0], params[1]);

            return null;
        }
    }

}



























