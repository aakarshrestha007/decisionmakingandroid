package chitchat.aakarshrestha.bottomnavigation.UI;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import chitchat.aakarshrestha.bottomnavigation.R;

public class ChatActivity extends AppCompatActivity {

    private TextView username_chatTextView;
    private ImageButton chat_activity_backArrow;
    private RelativeLayout relativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        username_chatTextView = (TextView) findViewById(R.id.username_chatTextViewID);
        chat_activity_backArrow = (ImageButton) findViewById(R.id.chat_activity_backArrow);
        relativeLayout = (RelativeLayout) findViewById(R.id.chatRelativeLayoutID);

    }

    @Override
    protected void onResume() {
        super.onResume();

        Intent intent = getIntent();
        final String chatUserNameValue = intent.getStringExtra("chat_username");
        final String chatContactPhoneNumberValue = intent.getStringExtra("chat_contactPhoneNumber");
        final String chatFriendImage = intent.getStringExtra("chat_friend_image");
        String chatRecipient_FcmTokenValue = intent.getStringExtra("chat_recipient_fcm_token");

        username_chatTextView.setText(chatUserNameValue);
        Picasso.with(ChatActivity.this).load(chatFriendImage).into(new com.squareup.picasso.Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                relativeLayout.setBackground(new BitmapDrawable(getApplication().getResources(), bitmap));
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });

        username_chatTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getIntentValues(chatUserNameValue, chatContactPhoneNumberValue, chatFriendImage);
            }
        });

        chat_activity_backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getIntentValues(chatUserNameValue, chatContactPhoneNumberValue, chatFriendImage);
            }
        });

    }

    private void getIntentValues(String chatUserNameValue, String chatContactPhoneNumberValue, String chatFriendImage) {
        Intent goBackIntent = new Intent(ChatActivity.this, MessageRoomActivity.class);
        goBackIntent.putExtra("contactname", chatUserNameValue);
        goBackIntent.putExtra("contactPhoneNumber", chatContactPhoneNumberValue);
        goBackIntent.putExtra("friend_image", chatFriendImage);
        startActivity(goBackIntent);
    }
}
