package chitchat.aakarshrestha.bottomnavigation.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import chitchat.aakarshrestha.bottomnavigation.Fragments.MoreFragment;
import chitchat.aakarshrestha.bottomnavigation.Fragments.SettingFragment;
import chitchat.aakarshrestha.bottomnavigation.Helper.Utility;
import chitchat.aakarshrestha.bottomnavigation.R;

public class MoreFragmentAdapter extends RecyclerView.Adapter<MoreFragmentAdapter.ViewHolder> {


    Context mContext;
    List<String> listOfMoreItems = new ArrayList<>();

    public MoreFragmentAdapter(Context mContext, List<String> listOfMoreItems) {
        this.mContext = mContext;
        this.listOfMoreItems = listOfMoreItems;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.more_fragment_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.moreItemText.setText(listOfMoreItems.get(position));
        Utility.changeTextColor(holder.moreItemText, holder.moreItemText.getContext());
        holder.moreItemText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingFragment settingFragment = new SettingFragment();
                MoreFragment.mMoreFragment.getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left).replace(R.id.contentContainer, settingFragment).commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return listOfMoreItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView moreItemText;

        public ViewHolder(View itemView) {
            super(itemView);

            moreItemText = (TextView) itemView.findViewById(R.id.moreItemID);
        }
    }
}
