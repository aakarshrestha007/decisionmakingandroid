package chitchat.aakarshrestha.bottomnavigation.UI;

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import chitchat.aakarshrestha.bottomnavigation.Fragments.MessageRoomFragment;
import chitchat.aakarshrestha.bottomnavigation.R;

public class MessageRoomActivity extends AppCompatActivity {

    public static MessageRoomActivity mMessageRoomActivity;
    protected Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_room);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        mMessageRoomActivity = this;
    }

    @Override
    protected void onResume() {
        super.onResume();

        MessageRoomFragment messageRoomFragment = new MessageRoomFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.messageContentContainer, messageRoomFragment).commit();

    }

    public void showSnackBar(String snackMessage) {
        snackbar.make(findViewById(android.R.id.content), snackMessage, Snackbar.LENGTH_LONG)
                .setActionTextColor(Color.WHITE)
                .show();
    }

    public void hideTheSoftKeyBoard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}
