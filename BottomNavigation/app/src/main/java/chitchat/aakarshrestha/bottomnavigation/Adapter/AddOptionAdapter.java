package chitchat.aakarshrestha.bottomnavigation.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import chitchat.aakarshrestha.bottomnavigation.R;

public class AddOptionAdapter extends RecyclerView.Adapter<AddOptionAdapter.ViewHolder>{


    public Context mContext;
    public List<String> optionList = new ArrayList<>();

    public AddOptionAdapter(Context mContext, List<String> optionList) {
        this.mContext = mContext;
        this.optionList = optionList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.option_layout, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.optionTextView.setText(optionList.get(position));
        holder.deleteOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //remove the data from the list
                optionList.remove(position);
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return optionList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView optionTextView;
        public ImageView deleteOption;

        public ViewHolder(View itemView) {
            super(itemView);

            optionTextView = (TextView) itemView.findViewById(R.id.optionID);
            deleteOption = (ImageView) itemView.findViewById(R.id.deleteOptionID);

        }
    }
}
