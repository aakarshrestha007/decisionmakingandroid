package chitchat.aakarshrestha.bottomnavigation.Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import chitchat.aakarshrestha.bottomnavigation.Adapter.TextFragmentAdapter;
import chitchat.aakarshrestha.bottomnavigation.Helper.Config;
import chitchat.aakarshrestha.bottomnavigation.Helper.TextMessage;
import chitchat.aakarshrestha.bottomnavigation.R;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class TextMessageFragment extends Fragment {

    private TextView username_chatTextView;
    private ImageButton chat_activity_backArrow;
    private RelativeLayout relativeLayout;
    private ImageButton sendTextButton;
    private EditText messageEditText;
    private ProgressBar progressBar;
    private OkHttpClient okHttpClient;
    private SharedPreferences sharedPrefName, sharedPrefPhone;
    private List<TextMessage> mListOfMsg;
    private TextFragmentAdapter textFragmentAdapter;
    private ListView mListView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private IntentFilter textIntentFilter;

    BroadcastReceiver textBroadCastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            sharedPrefPhone = getActivity().getSharedPreferences(Config.SHAREDPREFERENCES_MY_PHONE_NUMBER, Context.MODE_PRIVATE);
            final String sender_phone = sharedPrefPhone.getString(Config.MY_PHONE_NUMBER_KEY, "");

            final String chatContactPhoneNumberValue = getArguments().getString("chat_contactPhoneNumber");

            if (!sender_phone.equals("null") && chatContactPhoneNumberValue != null) {
                refreshMessages(sender_phone, chatContactPhoneNumberValue, 0);
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.text_message_fragment, container, false);

        username_chatTextView = (TextView) view.findViewById(R.id.username_chatTextViewID);
        chat_activity_backArrow = (ImageButton) view.findViewById(R.id.chat_activity_backArrow);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.chatRelativeLayoutID);
        sendTextButton = (ImageButton) view.findViewById(R.id.imageButtonSendID);
        messageEditText = (EditText) view.findViewById(R.id.messageEditTextID);
        progressBar = (ProgressBar) view.findViewById(R.id.messageProgressBarID);
        mListView = (ListView) view.findViewById(R.id.textMsg_listViewID);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);

        okHttpClient = new OkHttpClient();
        mListOfMsg = new ArrayList<>();

        /*
        Add the action sent from the broadcast from MyFirebaseMessagingService file.
         */
        textIntentFilter = new IntentFilter();
        textIntentFilter.addAction(Config.FCM_TEXT_MESSAGE_RECEIVED_ACTION);

        /*
        Register the receiver
         */
        getActivity().registerReceiver(textBroadCastReceiver, textIntentFilter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        /*
        Register the receiver
         */
        getActivity().registerReceiver(textBroadCastReceiver, textIntentFilter);

    }

    @Override
    public void onPause() {
        super.onPause();
        /*
        Unregister the receiver
         */
        getActivity().unregisterReceiver(textBroadCastReceiver);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final int count=0;

        progressBar.setVisibility(View.GONE);

        sharedPrefName = getActivity().getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
        final String sender_name = sharedPrefName.getString(Config.SHAREDPREFERENCES_NAME, "");

        sharedPrefPhone = getActivity().getSharedPreferences(Config.SHAREDPREFERENCES_MY_PHONE_NUMBER, Context.MODE_PRIVATE);
        final String sender_phone = sharedPrefPhone.getString(Config.MY_PHONE_NUMBER_KEY, "");

        final String sender_fcm_token = FirebaseInstanceId.getInstance().getToken();

        final String chatUserNameValue = getArguments().getString("chat_username");
        final String chatContactPhoneNumberValue = getArguments().getString("chat_contactPhoneNumber");
        final String chatFriendImage = getArguments().getString("chat_friend_image");
        String chatRecipient_FcmTokenValue = getArguments().getString("chat_recipient_fcm_token");

        username_chatTextView.setText(chatUserNameValue);
        Picasso.with(getActivity().getApplicationContext()).load(chatFriendImage).into(new com.squareup.picasso.Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                relativeLayout.setBackground(new BitmapDrawable(getActivity().getApplicationContext().getResources(), bitmap));
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });


        username_chatTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getIntentValues(chatUserNameValue, chatContactPhoneNumberValue, chatFriendImage);
            }
        });

        chat_activity_backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getIntentValues(chatUserNameValue, chatContactPhoneNumberValue, chatFriendImage);
            }
        });

        //load text messages
        refreshMessages(sender_phone, chatContactPhoneNumberValue, count);

        sendTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String textMessage = messageEditText.getText().toString().trim();
                if (!textMessage.isEmpty()) {

                    /*
                        Count the characters in the message
                         */
                    int counter = 0;
                    for (int i=0; i<textMessage.length(); i++) {
                        if ((Character.isLetterOrDigit(textMessage.charAt(i))) || (!Character.isLetterOrDigit(textMessage.charAt(i)))) {
                            counter++;
                        }
                    }

                    if (counter <= 140) {
                        PostTextMessageTask postTextMessageTask = new PostTextMessageTask();
                        postTextMessageTask.execute(sender_phone, chatContactPhoneNumberValue, sender_name, chatUserNameValue, textMessage.replace("'", "\''"), sender_fcm_token);
                        messageEditText.setText("");
                    } else {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity().getApplicationContext(), "Upto 140 characters are allowed!", Toast.LENGTH_LONG).show();
                            }
                        });
                    }

                }

                //load the text messages
                refreshMessages(sender_phone, chatContactPhoneNumberValue, count);

            }

        });

        //Refresh the listview and load the new data
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            int refreshCount = 0;
            @Override
            public void onRefresh() {
                refreshCount += 5;

                //load the text messages
                refreshMessages(sender_phone, chatContactPhoneNumberValue, refreshCount);

                Timer timer = new Timer();
                TimerTask task = new TimerTask() {
                    @Override
                    public void run() {
                        mListView.smoothScrollToPosition(0);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                swipeRefreshLayout.setRefreshing(false);
                            }
                        });
                    }
                };
                timer.schedule(task, 500);

            }

        });

    }

    private void postTextMessage(String sender_phone, String receiver_phone, String username_sender, String username_receiver, String message, String sender_fcm_token) {
        RequestBody body = new FormBody.Builder()
                .add("sender_phone", sender_phone)
                .add("receiver_phone", receiver_phone)
                .add("username_sender", username_sender)
                .add("username_receiver", username_receiver)
                .add("message", message)
                .add("sender_fcm_token", sender_fcm_token)
                .build();

        Request request = new Request.Builder()
                .url(Config.URL_TEXT_MESSAGE_SEND)
                .post(body)
                .build();

        try {
            Response response = okHttpClient.newCall(request).execute();
            if (!response.isSuccessful()) {
                throw new IOException("Unexpected code " + response);
            } else {
                String mResponse = response.body().string();
                JSONObject jsonObject = new JSONObject(mResponse);

                final String successMessage = jsonObject.getString("success");
                if (!successMessage.equals("Text message sent successfully!")) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity().getApplicationContext(), "Problem sending text, please try again!", Toast.LENGTH_LONG).show();
                        }
                    });
                }

            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

    }

    private class PostTextMessageTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            postTextMessage(params[0],params[1],params[2],params[3],params[4],params[5]);
            return null;
        }
    }

    private class GetTextMessageTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            getMessages(params[0], params[1], Integer.parseInt(params[2]));
            return null;
        }
    }

    private void getMessages(final String sender_phone, String receiver_phone, int limitNum) {

        Request request = new Request.Builder()
                .url(Config.urlToGetTextMessages(sender_phone, receiver_phone, limitNum))
                .build();

        try {
            Response response = okHttpClient.newCall(request).execute();

            if (response.isSuccessful()) {

                String mResponse = response.body().string();

                JSONArray jsonArray = new JSONArray(mResponse);

                TextMessage[] textMessages = new TextMessage[jsonArray.length()];

                for (int i=0; i<jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    TextMessage textMessage = new TextMessage();
                    textMessage.setMessage(jsonObject.getString("message"));
                    textMessage.setSenderPhone(jsonObject.getString("sender_phone"));

                    textMessages[i] = textMessage;
                }

                mListOfMsg = getMessageListValue(textMessages);
                Collections.reverse(mListOfMsg);

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textFragmentAdapter = new TextFragmentAdapter(getActivity().getApplicationContext(), mListOfMsg, sender_phone);
                        mListView.setAdapter(textFragmentAdapter);
                        textFragmentAdapter.notifyDataSetChanged();
                    }
                });

            }

            response.body().close();

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

    }

    private List<TextMessage> getMessageListValue(TextMessage[] messages) {

        List<TextMessage> list = Arrays.asList(messages);
        return list;

    }

    private void refreshMessages(String sender_phone, String chatContactPhoneNumberValue, int limitNum) {
        //load the text messages
        GetTextMessageTask getTextMessageTask = new GetTextMessageTask();
        getTextMessageTask.execute(sender_phone, chatContactPhoneNumberValue, String.valueOf(limitNum));
    }

    private void getIntentValues(String chatUserNameValue, String chatContactPhoneNumberValue, String chatFriendImage) {

        MessageRoomFragment messageRoomFragment = new MessageRoomFragment();

        Bundle chatBundle = new Bundle();
        chatBundle.putString("contactname", chatUserNameValue);
        chatBundle.putString("contactPhoneNumber", chatContactPhoneNumberValue);
        chatBundle.putString("friend_image", chatFriendImage);
        messageRoomFragment.setArguments(chatBundle);

        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right).replace(R.id.messageContentContainer, messageRoomFragment).commit();

    }
}
