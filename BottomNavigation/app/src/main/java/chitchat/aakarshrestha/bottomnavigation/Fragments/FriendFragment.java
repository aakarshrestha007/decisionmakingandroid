package chitchat.aakarshrestha.bottomnavigation.Fragments;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import chitchat.aakarshrestha.bottomnavigation.Adapter.ContactListAdapter;
import chitchat.aakarshrestha.bottomnavigation.Helper.Config;
import chitchat.aakarshrestha.bottomnavigation.Helper.FriendContact;
import chitchat.aakarshrestha.bottomnavigation.Helper.Utility;
import chitchat.aakarshrestha.bottomnavigation.R;
import chitchat.aakarshrestha.bottomnavigation.Service.MyFirebaseMessagingService;
import chitchat.aakarshrestha.bottomnavigation.UI.MainActivity;
import chitchat.aakarshrestha.bottomnavigation.UI.MessageRoomActivity;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class FriendFragment extends Fragment {

    private EditText searchEditText;
    private ListView listView;
    private FloatingActionButton goup_floating_button;
    public List<FriendContact> listOfNamesOne = new ArrayList<>();
    public ContactListAdapter contactListAdapter;
    public OkHttpClient okHttpClient = new OkHttpClient();

    public SharedPreferences mSharedPrefName, mSharedPrefPhoneNumber;

    public Uri uriContact;

    public CoordinatorLayout friendFragmentLayout;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.friend_fragment, container, false);

        searchEditText = (EditText) view.findViewById(R.id.friend_fragment_search_field_ID);
        listView = (ListView) view.findViewById(R.id.contact_ID);
        goup_floating_button = (FloatingActionButton) view.findViewById(R.id.goup_floatingButton_ID);
        goup_floating_button.setVisibility(View.INVISIBLE);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS) == PackageManager.GET_PERMISSIONS) {

                Timer timer = new Timer();
                TimerTask timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        RetrieveContactNameTask retrieveContactNameTask = new RetrieveContactNameTask();
                        retrieveContactNameTask.execute();
                    }
                };
                timer.schedule(timerTask, 65);

//                retrieveContactName();
            } else {

                if (shouldShowRequestPermissionRationale(Manifest.permission.READ_CONTACTS)) {
                    Toast.makeText(getActivity(), "Permission to access contacts is needed to use this app!", Toast.LENGTH_LONG).show();
                }

                requestPermissions(new String[] {Manifest.permission.READ_CONTACTS}, 0);
            }

        } else {
            Timer timer = new Timer();

            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    RetrieveContactNameTask retrieveContactNameTask = new RetrieveContactNameTask();
                    retrieveContactNameTask.execute();
                }
            };
            timer.schedule(timerTask, 65);
//            retrieveContactName();
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final String friendName = listOfNamesOne.get(position).getFriendName();
                final String friendPhoneNum = listOfNamesOne.get(position).getFriendPhoneNumber();
                final String friendImage = listOfNamesOne.get(position).getFriendImage();

                String thisIsMyPhoneNumber = MainActivity.mMainActivity.getMyPhoneNumber().replaceAll("\\D+","");

                if (friendPhoneNum.replaceAll("\\D+","").equals(thisIsMyPhoneNumber)) {
                    Toast.makeText(getContext(), "If you are lonely, talk to your friends! ☺ \uD83D\uDC8B", Toast.LENGTH_LONG).show();
                } else {

                    Request request = new Request.Builder()
                            .url(Config.URL_FRIEND_EXISTENCE + friendPhoneNum.replaceAll("\\D+",""))
                            .build();

                    try {
                        Response response = okHttpClient.newCall(request).execute();

                        String mResponse = response.body().string();
                        if (response.isSuccessful()) {
                            JSONObject jsonObject = new JSONObject(mResponse);
                            String friendExistenceValue = jsonObject.getString("friendExistStatus");

                            if (friendPhoneNum.replaceAll("\\D+","").equals(friendExistenceValue)) {

                                String user_one_fcm_token = FirebaseInstanceId.getInstance().getToken();
                                String user_two_fcm_token = jsonObject.getString("user_two_fcm_token");

                                //Add the user to the friend list in the background using AsynTask
                                mSharedPrefPhoneNumber = getActivity().getSharedPreferences(Config.SHAREDPREFERENCES_MY_PHONE_NUMBER, Context.MODE_PRIVATE);
                                final String myPhoneNumber = mSharedPrefPhoneNumber.getString(Config.MY_PHONE_NUMBER_KEY, "");

                                mSharedPrefName = getActivity().getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
                                final String myName = mSharedPrefName.getString(Config.SHAREDPREFERENCES_NAME, "");

                                AddUserToFriendListTask addUserToFriendListTask = new AddUserToFriendListTask();
                                addUserToFriendListTask.execute(myName, myPhoneNumber, friendName, friendPhoneNum, user_one_fcm_token, user_two_fcm_token);

                            /*
                            if the phone number matches, start the messaging action else navigate to Contact Detail page
                            to send invitation to the app.
                             */
                                Intent intent = new Intent(getContext(), MessageRoomActivity.class);
                                intent.putExtra("contactname", friendName);
                                intent.putExtra("contactPhoneNumber", friendPhoneNum);
                                intent.putExtra("friend_image", friendImage);
                                intent.putExtra("recipient_fcm_token", user_two_fcm_token);
                                startActivity(intent);

                                FriendFragment.this.getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

                            } else {
                                ContactDetailFragment contactDetailFragment = new ContactDetailFragment();
                                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left).replace(R.id.contentContainer, contactDetailFragment).commit();

                            /*
                            To move data from fragment to fragment, use Bundle
                             */
                                Bundle bundle = new Bundle();
                                bundle.putString("contactname", friendName);
                                bundle.putString("contactPhoneNumber", friendPhoneNum);
                                bundle.putString("contactImage", friendImage);
                                bundle.putInt("pos", position);
                                contactDetailFragment.setArguments(bundle);
                            }
                        }

                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }

                }



            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                if (listView.getFirstVisiblePosition() != 0) {
                    goup_floating_button.setVisibility(View.VISIBLE);
                } else {
                    goup_floating_button.setVisibility(View.INVISIBLE);
                }

            }
        });

        goup_floating_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listView.smoothScrollToPosition(0);
            }
        });

        friendFragmentLayout = (CoordinatorLayout) view.findViewById(R.id.friend_fragment_ID);
        Utility.changeBackgroundColor(friendFragmentLayout, getContext());

        /*
            Search functionality
         */

        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = searchEditText.getText().toString().toLowerCase(Locale.getDefault());
                contactListAdapter.filter(text);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

        /*Scroll to the last position of the item clicked */
        Bundle arguments = getArguments();
        if (arguments != null) {
            Timer timer = new Timer();
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    int positionOfItem = getArguments().getInt("positionOfItem");
                    listView.smoothScrollToPosition(positionOfItem);
                }
            };
            timer.schedule(timerTask, 200);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Timer timer = new Timer();

                TimerTask timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        retrieveContactName();
                    }
                };
                timer.schedule(timerTask, 55);
            } else {
                Toast.makeText(getActivity(), "Permission to access contacts was denied before!", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void retrieveContactName() {

        String phoneName, phoneNumber;
        String friend_image_uri;
        Context context = FriendFragment.this.getContext();
        ContentResolver cr = context.getContentResolver();

        uriContact = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection    = new String[] {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.PHOTO_URI};
        Cursor phone = cr.query(uriContact, projection, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");

        assert phone != null;
        while (phone.moveToNext()) {

            FriendContact friendContact = new FriendContact();

            phoneName = phone.getString(phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            phoneNumber = phone.getString(phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            friend_image_uri = phone.getString(phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));

            System.out.println("Photo image " + friend_image_uri);

            friendContact.setFriendName(phoneName);
            friendContact.setFriendPhoneNumber(phoneNumber);
            friendContact.setFriendImage(friend_image_uri);

            listOfNamesOne.add(friendContact);

        }

        phone.close();

         /*
        Setup the adapter
         */

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                contactListAdapter = new ContactListAdapter(getContext(), listOfNamesOne);
                listView.setAdapter(contactListAdapter);
                contactListAdapter.notifyDataSetChanged();
            }
        });

    }

    private class RetrieveContactNameTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            retrieveContactName();
            return null;
        }
    }

    public void addUserToFriendList(String user_one_name, String user_one_phone, String user_two_name, String user_two_phone, String user_one_fcm_token, String user_two_fcm_token) {

        RequestBody requestBody = new FormBody.Builder()
                .add("user_one_name", user_one_name)
                .add("user_one_phone", user_one_phone.replaceAll("\\D+",""))
                .add("user_two_name", user_two_name)
                .add("user_two_phone", user_two_phone.replaceAll("\\D+",""))
                .add("user_one_fcm_token", user_one_fcm_token)
                .add("user_two_fcm_token", user_two_fcm_token)
                .build();

        Request request = new Request.Builder()
                .url(Config.URL_LIST_OF_FRIENDS)
                .post(requestBody)
                .build();

        try {
            Response response = okHttpClient.newCall(request).execute();

            if (response.isSuccessful()) {

                String mResponse = response.body().string();
                JSONObject jsonObject = new JSONObject(mResponse);

                if (jsonObject.names().get(0).equals("success")) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getContext(), "Added to the friend list successfully!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            } else {
                throw new IOException("Unexpected code " + response);
            }

            response.body().close();

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

    }

    private class AddUserToFriendListTask extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) {
            addUserToFriendList(params[0], params[1], params[2], params[3], params[4], params[5]);
            return null;
        }
    }
}
