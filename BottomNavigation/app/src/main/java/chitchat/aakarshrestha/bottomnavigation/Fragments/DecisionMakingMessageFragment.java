package chitchat.aakarshrestha.bottomnavigation.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import chitchat.aakarshrestha.bottomnavigation.R;

public class DecisionMakingMessageFragment extends Fragment{

    protected ImageView cardImageView;
    protected ImageButton button1, button2, button3, button4;
    protected TextView textView;

    boolean isBackOfCardShown = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.decision_making_message_fragment, container, false);

        button1 = (ImageButton) view.findViewById(R.id.imageButton0);
        button2 = (ImageButton) view.findViewById(R.id.imageButton1);
        button3 = (ImageButton) view.findViewById(R.id.imageButton2);
        button4 = (ImageButton) view.findViewById(R.id.imageButton3);

        cardImageView = (ImageView) view.findViewById(R.id.cardImageID);
        textView = (TextView) view.findViewById(R.id.textView);
        textView.setVisibility(View.GONE);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = "Sushi";
                flipTheCard(text);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = "MoMo";
                flipTheCard(text);
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = "BBQ";
                flipTheCard(text);
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = "Burger";
                flipTheCard(text);
            }
        });


        return view;

    }

    public void flipTheCard(String text) {
        if (isBackOfCardShown == false) {
            cardImageView.setImageResource(R.drawable.crackedeasteregg);
            YoYo.with(Techniques.DropOut).duration(700).playOn(cardImageView);
            isBackOfCardShown = true;
            textView.setVisibility(View.VISIBLE);
            textView.setText(text);
            YoYo.with(Techniques.FadeOut).duration(10).playOn(textView);
            YoYo.with(Techniques.FadeIn).duration(900).playOn(textView);
            YoYo.with(Techniques.BounceInUp).duration(2000).playOn(textView);
            //Toast.makeText(getApplicationContext(), "You cracked the EGG!", Toast.LENGTH_SHORT).show();
        } else {
            cardImageView.setImageResource(R.drawable.eastergoldegg);
            YoYo.with(Techniques.Tada).duration(700).playOn(cardImageView);
            textView.setVisibility(View.GONE);
            textView.setText("");
            isBackOfCardShown = false;
        }
    }
}

















