package chitchat.aakarshrestha.bottomnavigation.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import chitchat.aakarshrestha.bottomnavigation.Helper.ProfileMessageList;
import chitchat.aakarshrestha.bottomnavigation.Helper.Utility;
import chitchat.aakarshrestha.bottomnavigation.R;

public class ProfileMessageListAdapter extends BaseAdapter{

    private Context mContext;
    private List<ProfileMessageList> listOfMessages = new ArrayList<>();
    private LayoutInflater layoutInflater;

    private String myPhoneNumber;

    public ProfileMessageListAdapter(Context mContext, List<ProfileMessageList> listOfMessages, String myPhoneNumber) {
        this.mContext = mContext;
        this.listOfMessages = listOfMessages;
        this.myPhoneNumber = myPhoneNumber;
        layoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return listOfMessages.size();
    }

    @Override
    public Object getItem(int position) {
        return listOfMessages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {

            convertView = layoutInflater.inflate(R.layout.profile_message_list_item_from, null);
            viewHolder = new ViewHolder();
            viewHolder.usernameOfMessage = (TextView) convertView.findViewById(R.id.nameID);
            viewHolder.question = (TextView) convertView.findViewById(R.id.profile_questionID);
            viewHolder.dateView = (TextView) convertView.findViewById(R.id.profile_date_id);
            viewHolder.picked_option = (TextView) convertView.findViewById(R.id.profile_picked_optionID);
            viewHolder.starImageView = (ImageView) convertView.findViewById(R.id.starImageViewID);
            viewHolder.cardView = (CardView) convertView.findViewById(R.id.cardView_profile_ID);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        ProfileMessageList profileMessageList = listOfMessages.get(position);

        String nameOfMessageReceiver = profileMessageList.getSenderName();
        String questionAsked = profileMessageList.getQuestion();
        String picked_option_value = profileMessageList.getOption_picked();
        String dateOnMessage = profileMessageList.getCreatedDate();

        String niceDateValueFormat = Utility.niceDateFormatter(dateOnMessage);

        viewHolder.usernameOfMessage.setText(nameOfMessageReceiver);
        viewHolder.question.setText(questionAsked);
        viewHolder.usernameOfMessage.setTextColor(Color.parseColor("#1e8fb9"));
        viewHolder.question.setTextColor(Color.parseColor("#1e8fb9"));
        viewHolder.picked_option.setText(picked_option_value);
        viewHolder.picked_option.setTextColor(Color.parseColor("#008080"));
        viewHolder.dateView.setText(niceDateValueFormat);
        viewHolder.dateView.setTextColor(Color.parseColor("#C0C0C0"));
        
        Utility.changeTextColor(viewHolder.usernameOfMessage, mContext);
        Utility.changeTextColor(viewHolder.question, mContext);
        Utility.changeCardViewColor(viewHolder.cardView, mContext);

        Picasso.with(mContext).load(R.drawable.star).into(viewHolder.starImageView);

        return convertView;
    }


    static class ViewHolder {

        TextView usernameOfMessage;
        TextView question;
        TextView dateView;
        TextView picked_option;
        ImageView starImageView;
        CardView cardView;

    }
}
