package chitchat.aakarshrestha.bottomnavigation.Helper;

public class ProfileMessageList {

    String senderName, receiverName, question, senderPhoneNumber, receiverPhoneNumber, createdDate, option_picked;

    public ProfileMessageList() {
    }

    public ProfileMessageList(String senderName, String receiverName, String question, String senderPhoneNumber, String receiverPhoneNumber, String createdDate, String option_picked) {
        this.senderName = senderName;
        this.receiverName = receiverName;
        this.question = question;
        this.senderPhoneNumber = senderPhoneNumber;
        this.receiverPhoneNumber = receiverPhoneNumber;
        this.createdDate = createdDate;
        this.option_picked = option_picked;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getSenderPhoneNumber() {
        return senderPhoneNumber;
    }

    public void setSenderPhoneNumber(String senderPhoneNumber) {
        this.senderPhoneNumber = senderPhoneNumber;
    }

    public String getReceiverPhoneNumber() {
        return receiverPhoneNumber;
    }

    public void setReceiverPhoneNumber(String receiverPhoneNumber) {
        this.receiverPhoneNumber = receiverPhoneNumber;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getOption_picked() {
        return option_picked;
    }

    public void setOption_picked(String option_picked) {
        this.option_picked = option_picked;
    }

    public String toString() {
        return senderName + "\n"
                + receiverName + "\n"
                + question + "\n"
                + senderPhoneNumber + "\n"
                + receiverPhoneNumber + "\n"
                + createdDate + "\n"
                + option_picked;
    }
}
