package chitchat.aakarshrestha.bottomnavigation.UI;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.Window;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.google.firebase.iid.FirebaseInstanceId;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnMenuTabClickListener;

import java.io.IOException;
import java.util.List;
import java.util.TimerTask;

import chitchat.aakarshrestha.bottomnavigation.Fragments.FriendFragment;
import chitchat.aakarshrestha.bottomnavigation.Fragments.IntroToApp;
import chitchat.aakarshrestha.bottomnavigation.Fragments.MessageFragment;
import chitchat.aakarshrestha.bottomnavigation.Fragments.MoreFragment;
import chitchat.aakarshrestha.bottomnavigation.Fragments.ProfileFragment;
import chitchat.aakarshrestha.bottomnavigation.Helper.BuildVersionCheck;
import chitchat.aakarshrestha.bottomnavigation.Helper.Config;
import chitchat.aakarshrestha.bottomnavigation.Helper.DataBaseHelper;
import chitchat.aakarshrestha.bottomnavigation.R;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    public static MainActivity mMainActivity;

    public SharedPreferences mSharedPrefMyNumber;
    public SharedPreferences.Editor mEditorMyNumber;
    public SharedPreferences mIntroAppSharedPreferences;

    protected OkHttpClient okHttpClient = new OkHttpClient();
    public boolean isPhoneNumberDisplayed = false;

    //checking the build version
    BuildVersionCheck mBuildVersionCheck;

    private BottomBar bottomBar;

    String phoneNumber;

    DataBaseHelper dataBaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);

        mBuildVersionCheck = new BuildVersionCheck();
        mBuildVersionCheck.checkBuildVersion();

        /*
        Intro to app fragments
         */
        mIntroAppSharedPreferences = getSharedPreferences(Config.SHAREDPREFERENCES_INTRO_TO_APP, Context.MODE_PRIVATE);
        String introToAppIsDisplayedValue = mIntroAppSharedPreferences.getString(Config.INTRO_TO_APP_KEY_VALUE, "");

        if (!introToAppIsDisplayedValue.equals("yes")) {
            Intent introAppIntent = new Intent(getApplicationContext(), IntroToApp.class);
            startActivity(introAppIntent);
        }


        bottomBar = BottomBar.attach(this, savedInstanceState, Color.parseColor("#00BBD3"), ContextCompat.getColor(this, R.color.white), 0.45f);
        bottomBar.setItems(R.menu.bottom_navigation_menu);
        bottomBar.setBackgroundColor(Color.parseColor("#FFFFFF"));

        bottomBar.setOnMenuTabClickListener(new OnMenuTabClickListener() {
            @Override
            public void onMenuTabSelected(@IdRes int menuItemId) {
                switch (menuItemId) {

                    case R.id.tab_message:
                        MessageFragment messageFragment = new MessageFragment();
                        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right).replace(R.id.contentContainer, messageFragment).commit();
                        break;

                    case R.id.tab_contacts:
                        FriendFragment friendFragment = new FriendFragment();
                        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left).replace(R.id.contentContainer, friendFragment).commit();
                        break;

                    case R.id.tab_profile:
                        ProfileFragment profileFragment = new ProfileFragment();
                        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right).replace(R.id.contentContainer, profileFragment).commit();
                        break;

                    case R.id.tab_more:
                        MoreFragment moreFragment = new MoreFragment();
                        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left).replace(R.id.contentContainer, moreFragment).commit();
                        break;
                }
            }

            @Override
            public void onMenuTabReSelected(@IdRes int menuItemId) {

            }
        });

        mMainActivity = this;

        /*
        Facebook login
         */

        if (AccessToken.getCurrentAccessToken() == null) {
            goToLoginActivity();
        }

    }

    public void goToLoginActivity() {

        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();

        /*
        Check permission
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_PHONE_STATE) ==
                    PackageManager.PERMISSION_GRANTED) {
                getMyPhoneNumber();
                phoneNumber = getMyPhoneNumber();

            } else {
                if (shouldShowRequestPermissionRationale(Manifest.permission.READ_PHONE_STATE)) {
                    Toast.makeText(MainActivity.this, "Permission is need to access your phone!", Toast.LENGTH_LONG).show();
                }

                requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, 0);

            }

        } else {
            getMyPhoneNumber();
            phoneNumber = getMyPhoneNumber();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        /*
        Insert username and phone number in the database
         */
        Intent intent = getIntent();
        final String username = intent.getStringExtra(Config.FACEBOOK_NAME);
        final String phoneNumberFromMSGRoomActivity = intent.getStringExtra("my_phone_number");
        final String usernameFromMSGRoomActivity = intent.getStringExtra("fb_name");

        //save my phone number
        mSharedPrefMyNumber = getSharedPreferences(Config.SHAREDPREFERENCES_MY_PHONE_NUMBER, Context.MODE_PRIVATE);
        mEditorMyNumber = mSharedPrefMyNumber.edit();
        mEditorMyNumber.putString(Config.MY_PHONE_NUMBER_KEY, phoneNumber);
        mEditorMyNumber.apply();

        /**
         * Implementing Firebase Cloud Messaging Notification and getting the fcm registration token
         */
        final String fcmToken = FirebaseInstanceId.getInstance().getToken();
        //Log.v("fcmToken", fcmToken);

        if(phoneNumber != null) {
            phoneNumber.replaceAll("\\D+","");
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!isPhoneNumberDisplayed) {

                    if (username != null && phoneNumber != null) {
                        if (postUsernameAndPhone(username, phoneNumber, fcmToken)) {
                            isPhoneNumberDisplayed = false;
                        } else {
                            Toast.makeText(getApplicationContext(), "Error Posting!", Toast.LENGTH_LONG).show();
                        }
                    }

                    if (usernameFromMSGRoomActivity != null && phoneNumberFromMSGRoomActivity != null) {
                        if (postUsernameAndPhone(usernameFromMSGRoomActivity, phoneNumberFromMSGRoomActivity.replaceAll("\\D+",""), fcmToken)) {
                            isPhoneNumberDisplayed = false;
                        }
                    }
                }

            }
        });

    }

    public String getMyPhoneNumber() {

        /*
        Getting my phone number
         */
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String myPhoneNumber = telephonyManager.getLine1Number();

        return myPhoneNumber;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getMyPhoneNumber();
            } else {
                Toast.makeText(getApplicationContext(), "Permission to access the phone was denied before!", Toast.LENGTH_LONG).show();
            }
        } else {
            getMyPhoneNumber();
            phoneNumber = getMyPhoneNumber();
        }
    }

    public boolean postUsernameAndPhone(String username, String phonenumber, String fcmToken) {

        boolean isPostUsernameAndPhoneSuccessful = false;

        RequestBody mBody = new FormBody.Builder()
                .add("username", username)
                .add("phonenumber", phonenumber)
                .add("fcm_reg_id", fcmToken)
                .build();

        Request request = new Request.Builder()
                .url(Config.URL_USERS)
                .post(mBody)
                .build();

        try {
            Response response = okHttpClient.newCall(request).execute();

            if (!response.isSuccessful()) {
                throw new IOException("Unexpected code " + response);
            } else {
                isPostUsernameAndPhoneSuccessful = true;
            }

            response.body().close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return isPostUsernameAndPhoneSuccessful;

    }

}
