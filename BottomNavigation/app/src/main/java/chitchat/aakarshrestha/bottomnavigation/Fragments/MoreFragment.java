package chitchat.aakarshrestha.bottomnavigation.Fragments;

import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import chitchat.aakarshrestha.bottomnavigation.Adapter.MoreFragmentAdapter;
import chitchat.aakarshrestha.bottomnavigation.Helper.Utility;
import chitchat.aakarshrestha.bottomnavigation.R;

public class MoreFragment extends Fragment{

    protected List<String> listOfMoreItems;
    protected MoreFragmentAdapter moreFragmentAdapter;

    public RecyclerView recyclerView;
    public LinearLayoutManager linearLayoutManager;
    public ConstraintLayout moreFragmentLayout;

    public static MoreFragment mMoreFragment;

    public TextView settingText, addContactText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.more_fragment, container, false);

        mMoreFragment = this;

        /*
        Night mode
         */
        moreFragmentLayout = (ConstraintLayout) view.findViewById(R.id.moreFragmentLayoutID);
        Utility.changeBackgroundColor(moreFragmentLayout, getContext());

        settingText = (TextView) view.findViewById(R.id.moreFragmentRecyclerView_ID);
        addContactText = (TextView) view.findViewById(R.id.add_contacts_ID);

        Utility.changeTextColor(settingText, getContext());
        Utility.changeTextColor(addContactText, getContext());

        settingText.setText("Settings");
        settingText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingFragment settingFragment = new SettingFragment();
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left).replace(R.id.contentContainer, settingFragment).commit();
            }
        });

        addContactText.setText("Add Contacts");
        addContactText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                Using the explicit intent couldn't be useful because it is import to have the same package/activity in all phones.
                 */
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.setData(Uri.parse("content://contacts/people/"));
                startActivity(i);
            }
        });


        return view;
    }
}
