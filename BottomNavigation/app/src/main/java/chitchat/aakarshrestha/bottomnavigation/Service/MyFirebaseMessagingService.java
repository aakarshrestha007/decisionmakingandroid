package chitchat.aakarshrestha.bottomnavigation.Service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import chitchat.aakarshrestha.bottomnavigation.Fragments.ProfileFragment;
import chitchat.aakarshrestha.bottomnavigation.Helper.Config;
import chitchat.aakarshrestha.bottomnavigation.Helper.DataBaseHelper;
import chitchat.aakarshrestha.bottomnavigation.R;
import chitchat.aakarshrestha.bottomnavigation.UI.MainActivity;
import chitchat.aakarshrestha.bottomnavigation.UI.MessageReceivedActivity;
import chitchat.aakarshrestha.bottomnavigation.UI.MessageRoomActivity;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMessagingService";
    final static String GROUP_KEY_MESSAGES = "group_key_messages";
    private final static int NOTIFICATION_ID = 1115;
    final static String GROUP_KEY_ANSWER = "group_key_answer";
    private final static int CONFIRM_NOTIFICATION_ID = 13;
    private final static int TEXT_MESSAGE_ID = 21;

    DataBaseHelper dataBaseHelper;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        dataBaseHelper = new DataBaseHelper(this);

//        Log.v(TAG, "FROM: " + remoteMessage.getFrom());

        //Check if the message contains data
        if (remoteMessage.getData().size() > 0) {
//            Log.v(TAG, "Message data: " + remoteMessage.getData());
        }

        //Check if the message contains notification
        if (remoteMessage.getNotification() != null) {
//            Log.v(TAG, "Message body: " + remoteMessage.getNotification().getBody());

            SharedPreferences sharedPreferences = getSharedPreferences(Config.SHAREDPREFERENCES_NOTIFICATION_MODE, Context.MODE_PRIVATE);
            String notificationSetToOn = sharedPreferences.getString(Config.NOTIFICATION_MODE_KEY_VALUE, "");

            String from = remoteMessage.getData().get("senderName");
            String message = remoteMessage.getData().get("message");
            String phoneNumber = remoteMessage.getData().get("senderPhone");
            String send_fcm_token = remoteMessage.getData().get("recipient_fcm_token");

            if (from != null && message != null && phoneNumber != null) {

                /*
                Verifying the notification is inserted in the sqlite database
                 */
                boolean isDataInserted = dataBaseHelper.insertAskNotifications(from, message);
                if (isDataInserted) {
                    Log.v("DataInsertedPassed", "Insertion of data passed!");
                } else {
                    Log.v("DataInsertedFailed", "Insertion of data failed!");
                }

                /*
                 Send notification if the Notification is set to ON
                */
                if (notificationSetToOn.equals("Notification Mode")) {
                    sendNotificationToAsk(from, message, phoneNumber, send_fcm_token);
                }

                /*
                Send broadcast once the notification is sent to refresh the MessageFragment
                 */
                Intent broadcastIntent = new Intent();
                broadcastIntent.setAction(Config.FCM_ASK_RECEIVED_ACTION);
                sendBroadcast(broadcastIntent);
            }

            String receiver_name = remoteMessage.getData().get("receiver_name");
            String confirmed_option = remoteMessage.getData().get("confirmed_option");
            String receiver_phone_number = remoteMessage.getData().get("receiver_phone_number");

            if (receiver_name != null && confirmed_option != null && receiver_phone_number != null) {

                /*
                Verifying the answer notification is inserted in the sqlite database
                 */

                boolean isDataInserted = dataBaseHelper.insertAnswerNotifications(receiver_name, confirmed_option, null);
                if (isDataInserted) {
                    Log.v("InsertedDataPassed", "Insertion of data passed!");
                } else {
                    Log.v("InsertedDataFailed", "Insertion of data failed!");
                }

                 /*
                Send notification if the Notification is set to ON
                 */
                if (notificationSetToOn.equals("Notification Mode")) {
                    sendNotificationToConfirm(receiver_name, confirmed_option);
                }

                /*
                Send broadcast once the notification is sent to refresh the MessageFragment
                 */
                Intent bCastIntent = new Intent();
                bCastIntent.setAction(Config.FCM_CONFIRM_RECEIVED_ACTION);
                sendBroadcast(bCastIntent);
            }

            String textMessage = remoteMessage.getData().get("textmessage");
            String textSenderName = remoteMessage.getData().get("textsenderName");
            String textSenderPhone = remoteMessage.getData().get("textsenderPhone");

            if (textMessage != null && textSenderName != null && textSenderPhone != null) {
                /*
                Verifying the answer notification is inserted in the sqlite database
                 */
                boolean isTextMessageDataInserted = dataBaseHelper.insertTextMessageNotification(textSenderName, textMessage);
                if (isTextMessageDataInserted) {
                    Log.v("InsertedTextMsgData", "Insertion of data passed!");
                } else {
                    Log.v("InsertedTextMsgData", "Insertion of data failed!");
                }

                sendTextMessageNotification(textSenderName, textMessage);

                /*
                Send broadcast once the notification is sent to refresh TextMessageFragment
                 */
                Intent textBroadCast = new Intent();
                textBroadCast.setAction(Config.FCM_TEXT_MESSAGE_RECEIVED_ACTION);
                sendBroadcast(textBroadCast);
            }

        }

    }

    private void sendNotificationToAsk(String from, String body, String phoneNumber, String send_fcm_token) {

        Intent intent = new Intent(getApplicationContext(), MessageReceivedActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("friend_name_from_list", from);
        intent.putExtra("friend_phone_number_from_list", phoneNumber);
        intent.putExtra("recipient_fcm_token", send_fcm_token);
        intent.putExtra("askNotificationKey", "1");

        PendingIntent pendingIntent = PendingIntent.getActivity(this, NOTIFICATION_ID, intent, PendingIntent.FLAG_ONE_SHOT);

        //Set sound of notification
        Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        /*
        Pull all notifications from the sqlite database
         */

        NotificationCompat.InboxStyle style = new NotificationCompat.InboxStyle();

        Cursor result = dataBaseHelper.getAllNotification();

        List<String> listOfNotifications = new ArrayList<>();

        if (result.getCount() > 0) {

            while (result.moveToNext()) {
                listOfNotifications.add(result.getString(0) + " is asking: " + result.getString(1));
            }

            for (int i=0; i<listOfNotifications.size(); i++) {
                style.addLine(listOfNotifications.get(i));
            }

        }

        int count = result.getCount();

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.sooru_logo)
                .setContentTitle(count + " new messages")
                .setContentText(from + " is asking: " + body)
                .setAutoCancel(true)
                .setGroup(GROUP_KEY_MESSAGES)
                .setGroupSummary(true)
                .setSound(notificationSound)
                .setContentIntent(pendingIntent)
                .setStyle(style);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify("DecisionMaker", NOTIFICATION_ID, notificationBuilder.build());

    }

    private void sendNotificationToConfirm(String from, String body) {

        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

        //Generate random number
//        Random random = new Random();
//        int requestCode = random.nextInt(9999 - 1000) + 1000;

        PendingIntent pendingIntent = PendingIntent.getActivity(this, CONFIRM_NOTIFICATION_ID, intent, PendingIntent.FLAG_ONE_SHOT);

        //Set sound of notification
        Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

         /*
        Pull all notifications from the sqlite database
         */

        NotificationCompat.InboxStyle style = new NotificationCompat.InboxStyle();

        Cursor result = dataBaseHelper.getAllAnswerNotification();

        List<String> listOfAnswerNotifications = new ArrayList<>();

        if (result.getCount() > 0) {

            while (result.moveToNext()) {
                listOfAnswerNotifications.add(result.getString(0) + " picked: " + result.getString(1));
            }

            for (int i=0; i<listOfAnswerNotifications.size(); i++) {
                style.addLine(listOfAnswerNotifications.get(i));
            }

        }

        int count = result.getCount();

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.sooru_logo)
                .setContentTitle(count + " new picks")
                .setContentText(from + " picked: " + body + "!")
                .setAutoCancel(true)
                .setGroup(GROUP_KEY_ANSWER)
                .setGroupSummary(true)
                .setSound(notificationSound)
                .setContentIntent(pendingIntent)
                .setStyle(style);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify("DecisionMaker", CONFIRM_NOTIFICATION_ID, notificationBuilder.build());

    }

    private void sendTextMessageNotification(String from, String body) {
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, TEXT_MESSAGE_ID, intent, PendingIntent.FLAG_ONE_SHOT);

        //Set sound of notification
        Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

         /*
        Pull all notifications from the sqlite database
         */

        NotificationCompat.InboxStyle style = new NotificationCompat.InboxStyle();

        Cursor result = dataBaseHelper.getAllTextMessageNotificaton();

        List<String> listOfTextMessageNotifications = new ArrayList<>();

        if (result.getCount() > 0) {

            while (result.moveToNext()) {
                listOfTextMessageNotifications.add(result.getString(0) + ": " + result.getString(1));
            }

            for (int i=0; i<listOfTextMessageNotifications.size(); i++) {
                style.addLine(listOfTextMessageNotifications.get(i));
            }

        }

        int count = result.getCount();

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.sooru_logo)
                .setContentTitle(count + " Messages")
                .setContentText(from + ": " + body)
                .setAutoCancel(true)
                .setGroup(GROUP_KEY_ANSWER)
                .setGroupSummary(true)
                .setSound(notificationSound)
                .setContentIntent(pendingIntent)
                .setStyle(style);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify("DecisionMaker", TEXT_MESSAGE_ID, notificationBuilder.build());
    }
}
