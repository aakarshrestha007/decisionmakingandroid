package chitchat.aakarshrestha.bottomnavigation.Fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;

import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import chitchat.aakarshrestha.bottomnavigation.Adapter.AddOptionAdapter;
import chitchat.aakarshrestha.bottomnavigation.Helper.Config;
import chitchat.aakarshrestha.bottomnavigation.Helper.Utility;
import chitchat.aakarshrestha.bottomnavigation.R;
import chitchat.aakarshrestha.bottomnavigation.UI.MainActivity;
import chitchat.aakarshrestha.bottomnavigation.UI.MessageRoomActivity;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MessageRoomFragment extends Fragment{

    private ImageView backButtonImage;
    private TextView friendNameText, chatTextView;
    private EditText questionText, optionText;
    private CircleImageView friendCircleImageView;
    private Button addButton, sendButton;

    private AddOptionAdapter addOptionAdapter;
    List<String> listOfNames;

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;

    private SharedPreferences mSharedPref;

    protected OkHttpClient okHttpClient = new OkHttpClient();

    protected ConstraintLayout messageRoomActivityLayout;

    private SharedPreferences chatIntroSharedPref;
    private SharedPreferences.Editor chatIntroEditor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.message_room_fragment, container, false);

        backButtonImage = (ImageView) view.findViewById(R.id.messageRoomFragmentBackArrowID);
        friendNameText = (TextView) view.findViewById(R.id.friendNameID);
        friendCircleImageView = (CircleImageView) view.findViewById(R.id.friendImageID);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        messageRoomActivityLayout = (ConstraintLayout) view.findViewById(R.id.messageroom_activity_ID);
        Utility.changeBackgroundColor(messageRoomActivityLayout, getContext());

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_message_room_ID);
        linearLayoutManager = new LinearLayoutManager(getContext());

        backButtonImage = (ImageView) view.findViewById(R.id.messageRoomFragmentBackArrowID);
        friendNameText = (TextView) view.findViewById(R.id.friendNameID);
        friendCircleImageView = (CircleImageView) view.findViewById(R.id.friendImageID);
        chatTextView = (TextView) view.findViewById(R.id.chatID);

        questionText = (EditText) view.findViewById(R.id.questionID);
//        Utility.changeBackgroundColor(questionText, getApplicationContext());
//        Utility.changeColorOfEditTextField(questionText, getApplicationContext());
        optionText = (EditText) view.findViewById(R.id.addOptionsID);
//        Utility.changeBackgroundColor(optionText, getApplicationContext());
//        Utility.changeColorOfEditTextField(optionText, getApplicationContext());

        addButton = (Button) view.findViewById(R.id.addButton);
        addButton.setTextColor(Color.WHITE);
        sendButton = (Button) view.findViewById(R.id.sendButtonID);
        sendButton.setTextColor(Color.WHITE);

        listOfNames = new ArrayList<>();

        return view;

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                backButtonImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        navigateToMainActivityNow();
                    }
                });

                friendNameText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        navigateToMainActivityNow();
                    }
                });

                Intent intent = getActivity().getIntent();
                final String contactNameInMsgRoom = intent.getStringExtra("contactname");
                final String contactNumberInMsgRoom = intent.getStringExtra("contactPhoneNumber");
                final String friendImage = intent.getStringExtra("friend_image");

                friendNameText.setText(contactNameInMsgRoom);
                friendNameText.setTextColor(Color.WHITE);

                if (friendImage == null) {
                    Picasso.with(getContext()).load(R.drawable.dummy).into(friendCircleImageView);
                } else {
                    Picasso.with(getContext()).load(friendImage).into(friendCircleImageView);
                }
            }
        });

        /*
        Check for chat alert
         */
        chatIntroSharedPref = getActivity().getSharedPreferences("CHATSHAREDPREFERENCES", Context.MODE_PRIVATE);
        String okOnChatIntroSeen = chatIntroSharedPref.getString("okOnChatIntroSeen", "");
        if (!okOnChatIntroSeen.equals("null") && !okOnChatIntroSeen.equals("yes")) {
            showChatAlertForTheFirstTime();
        }

        /*
        Post the friend connection data in the database
         */

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String question = questionText.getText().toString().trim() + "?";

                if (Utility.isConnectedToNetwork(getContext())) {
                    if (!question.equals("") && listOfNames.size() == 4) {

                        int counter = 0;
                        for (int i=0; i<question.length(); i++) {
                            if ((Character.isLetterOrDigit(question.charAt(i))) || (!Character.isLetterOrDigit(question.charAt(i)))) {
                                counter++;
                            }
                        }

                        if (counter <= 50) {
                            Intent intent = getActivity().getIntent();
                            final String contactNumberInMsgRoom = intent.getStringExtra("contactPhoneNumber").replaceAll("\\D+","");
                            final String receiver_name = intent.getStringExtra("contactname");
                            final String recipient_fcm_token = intent.getStringExtra("recipient_fcm_token");
                            String sender_fcm_token = FirebaseInstanceId.getInstance().getToken();
                            String myPhoneNumber = MainActivity.mMainActivity.getMyPhoneNumber();

                            mSharedPref = getActivity().getSharedPreferences(Config.SHAREDPREFERENCES, Context.MODE_PRIVATE);
                            String sender_name = mSharedPref.getString(Config.SHAREDPREFERENCES_NAME, "");

                            String option_item_one = listOfNames.get(0);
                            String option_item_two = listOfNames.get(1);
                            String option_item_three = listOfNames.get(2);
                            String option_item_four = listOfNames.get(3);

                            assert sender_fcm_token != null;
                            RequestBody requestBody = new FormBody.Builder()
                                    .add("user_one_phone", myPhoneNumber)
                                    .add("user_two_phone", contactNumberInMsgRoom)
                                    .add("sender_name", sender_name)
                                    .add("receiver_name", receiver_name)
                                    .add("message_status", "0") // message status "0" = message sent but it has not opened or read yet.
                                    .add("question", question)
                                    .add("option_one", option_item_one)
                                    .add("option_two", option_item_two)
                                    .add("option_three", option_item_three)
                                    .add("option_four", option_item_four)
                                    .add("sender_fcm_token", sender_fcm_token)
                                    //.add("recipient_fcm_token", recipient_fcm_token)
                                    .build();

                            Request request = new Request.Builder()
                                    .url(Config.URL_FRIEND_CONNECTION)
                                    .post(requestBody)
                                    .build();

                            try {
                                Response response = okHttpClient.newCall(request).execute();

                                if (!response.isSuccessful()) {
                                    throw new IOException("Unexpected code " + response);
                                } else {

                                    String mResponse = response.body().string();
                                    JSONObject jsonObject = new JSONObject(mResponse);

                                    String successMessage = jsonObject.getString("success");

                                    sendButton.setEnabled(false);

                                    if (!successMessage.equals("Already exists!")) {
                                        MessageRoomActivity.mMessageRoomActivity.showSnackBar(successMessage);
                                        //Toast.makeText(getApplicationContext(), successMessage, Toast.LENGTH_LONG).show();
                                        /*
                                        Change focus to questionText - EditTextView
                                         */
                                        questionText.requestFocus();
                                        sendButton.setEnabled(true);
                                    }

                            /*
                            Set all the fields empty
                             */
                                    questionText.setText("");
                                    questionText.setHint("Type your question here...");

                                    //remove all items from list and reset the adapter.
                                    resetTheAdapter();

                                }

                                response.body().close();

                            } catch (IOException | JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            YoYo.with(Techniques.RubberBand).duration(700).playOn(questionText);
                            MessageRoomActivity.mMessageRoomActivity.showSnackBar("Only 50 characters allowed in question!");
                            //Toast.makeText(getApplicationContext(), "Only 50 characters allowed in question!", Toast.LENGTH_LONG).show();
                        }


                    } else {
                        YoYo.with(Techniques.RubberBand).duration(700).playOn(questionText);
                        MessageRoomActivity.mMessageRoomActivity.showSnackBar("Both Question and Option fields are required!");
                        //Toast.makeText(getApplicationContext(), "Both Question and Option fields are required!", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Utility.showNetworkConnectionAlertMessage(getActivity().getApplicationContext(), "Network Connectivity", "Please connect to the network.");
                }




            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Utility.isConnectedToNetwork(getContext())) {
                    //add an adapter here for AddOptionAdapter
                    String myText = optionText.getText().toString().trim();

                    if (!myText.isEmpty()) {

                        if (listOfNames.size() > 3) {
                            /*
                            Hide the soft keyboard
                             */
                            MessageRoomActivity.mMessageRoomActivity.hideTheSoftKeyBoard();
                            optionText.setText("");
                            MessageRoomActivity.mMessageRoomActivity.showSnackBar("You have reached your limit of four.");

                        } else {
                            listOfNames.add(myText);
                            optionText.setText("");
                            getData();
                        }
                    } else {
                        MessageRoomActivity.mMessageRoomActivity.showSnackBar("Add some items!");
                        //Toast.makeText(getApplicationContext(), "Add some items!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Utility.showNetworkConnectionAlertMessage(getActivity().getApplicationContext(), "Network Connectivity", "Please connect to the network.");
                }

            }

        });


        Intent intent = getActivity().getIntent();
        final String chatUserName = intent.getStringExtra("contactname");
        final String chatContactPhoneNumber = intent.getStringExtra("contactPhoneNumber");
        final String chatFriendImage = intent.getStringExtra("friend_image");
        final String chatRecipient_FcmToken = intent.getStringExtra("recipient_fcm_token");

        chatTextView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                MessageRoomActivity.mMessageRoomActivity.hideTheSoftKeyBoard();
                //make sure that the intent has some value, if the values are not there, use the bundle data coming
                //from chatFragment class
                if (chatUserName == null || chatContactPhoneNumber == null || chatRecipient_FcmToken == null) {
                    String chatUserName1 = getArguments().getString("contactname");
                    String chatContactPhoneNumber1 = getArguments().getString("contactPhoneNumber");
                    String chatFriendImage1 = getArguments().getString("friend_image");
                    if (chatFriendImage1 == null) {
                        chatFriendImage1 = getResources().getDrawable(R.drawable.dummy, null).toString();
                    }
                    String chatRecipient_FcmToken1 = getArguments().getString("recipient_fcm_token");

                    sendDataViaIntent(chatUserName1, chatContactPhoneNumber1, chatFriendImage1, chatRecipient_FcmToken1);
                } else {
                    String chatFriendImageNewValue;
                    if (chatFriendImage == null) {
                        chatFriendImageNewValue = getResources().getDrawable(R.drawable.dummy, null).toString();
                        sendDataViaIntent(chatUserName, chatContactPhoneNumber, chatFriendImageNewValue, chatRecipient_FcmToken);
                    } else {
                        sendDataViaIntent(chatUserName, chatContactPhoneNumber, chatFriendImage, chatRecipient_FcmToken);
                    }
                }

            }
        });

        friendCircleImageView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                MessageRoomActivity.mMessageRoomActivity.hideTheSoftKeyBoard();

                if (chatUserName == null || chatContactPhoneNumber == null || chatRecipient_FcmToken == null) {
                    String chatUserName1 = getArguments().getString("contactname");
                    String chatContactPhoneNumber1 = getArguments().getString("contactPhoneNumber");
                    String chatFriendImage1 = getArguments().getString("friend_image");
                    if (chatFriendImage1 == null) {
                        chatFriendImage1 = getResources().getDrawable(R.drawable.dummy, null).toString();
                    }
                    String chatRecipient_FcmToken1 = getArguments().getString("recipient_fcm_token");

                    sendDataViaIntent(chatUserName1, chatContactPhoneNumber1, chatFriendImage1, chatRecipient_FcmToken1);
                } else {
                    String chatFriendImageNewValue;
                    if (chatFriendImage == null) {
                        chatFriendImageNewValue = getResources().getDrawable(R.drawable.dummy, null).toString();
                        sendDataViaIntent(chatUserName, chatContactPhoneNumber, chatFriendImageNewValue, chatRecipient_FcmToken);
                    } else {
                        sendDataViaIntent(chatUserName, chatContactPhoneNumber, chatFriendImage, chatRecipient_FcmToken);
                    }
                }
            }
        });
    }

    public void navigateToMainActivityNow() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_CONTACTS) == PackageManager.GET_PERMISSIONS) {

            } else {
                String[] permissionRequested = {Manifest.permission.READ_CONTACTS};
                requestPermissions(permissionRequested, 0);
            }

        }

        Intent intent = getActivity().getIntent();
        String fb_name = intent.getStringExtra(Config.FACEBOOK_NAME);
        String fb_id = intent.getStringExtra(Config.FACEBOOK_ID);
        String fb_profile_image = intent.getStringExtra(Config.FACEBOOK_PROFILE_IMAGE);

        String phoneNumber = MainActivity.mMainActivity.getMyPhoneNumber();

        Intent intentToMainActivity = new Intent(getContext(), MainActivity.class);
        intent.putExtra("fb_name", fb_name);
        intent.putExtra("fb_id", fb_id);
        intent.putExtra("fb_profile_image", fb_profile_image);
        intent.putExtra("my_phone_number", phoneNumber);
        startActivity(intentToMainActivity);

        getActivity().overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    private void getData() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Collections.reverse(listOfNames);
                addOptionAdapter = new AddOptionAdapter(getContext(), listOfNames);
                recyclerView.setAdapter(addOptionAdapter);
                recyclerView.setLayoutManager(linearLayoutManager);
                addOptionAdapter.notifyDataSetChanged();
            }
        });
    }

    private void resetTheAdapter() {
        listOfNames.clear();
        recyclerView.setAdapter(addOptionAdapter);
        addOptionAdapter.notifyDataSetChanged();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            }
        }
    }

    private void sendDataViaIntent (String chatUserName, String chatContactPhoneNumber, String chatFriendImage, String chatRecipient_FcmToken) {

        TextMessageFragment chatFragment = new TextMessageFragment();

        Bundle bundle = new Bundle();
        bundle.putString("chat_username", chatUserName);
        bundle.putString("chat_contactPhoneNumber", chatContactPhoneNumber);
        bundle.putString("chat_friend_image", chatFriendImage);
        bundle.putString("chat_recipient_fcm_token", chatRecipient_FcmToken);
        chatFragment.setArguments(bundle);

        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left).replace(R.id.messageContentContainer, chatFragment).commit();

    }

    private void showChatAlertForTheFirstTime() {
        Timer timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                        View mView = layoutInflater.inflate(R.layout.show_chat_alert_intro, null);
                        TextView okText = (TextView) mView.findViewById(R.id.okTextID);
                        builder.setView(mView);

                        final AlertDialog dialog = builder.create();

                        okText.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();

                                chatIntroSharedPref = getActivity().getSharedPreferences("CHATSHAREDPREFERENCES", Context.MODE_PRIVATE);
                                chatIntroEditor = chatIntroSharedPref.edit();
                                chatIntroEditor.putString("okOnChatIntroSeen", "yes");
                                chatIntroEditor.apply();

                            }
                        });

                        dialog.show();
                    }
                });
            }
        };
        timer.schedule(timerTask, 550);
    }
}
